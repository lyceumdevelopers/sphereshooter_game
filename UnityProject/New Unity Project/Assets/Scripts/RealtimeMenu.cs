﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Vehicles.Ball;

public class RealtimeMenu : NetworkBehaviour {

	bool isActive = false;
	Rect[] _Button = new Rect[5];
	const float _WIDTH = 250, _HEIGHT = 50;
	public enum Language { Russian, English }
	public Language _Language = Language.Russian;
	NetworkManager manager;
    BallUserControl ballUserControl;
    WeaponController weaponController;
	// Use this for initialization
	void Start()
	{
		manager = GameObject.Find ("NetworkManager").GetComponent<NetworkManager> ();
		for (int i = 0; i < _Button.GetLength(0); i++)
		{
			_Button[i] = new Rect(_WIDTH / 2, 25 + (i * (_HEIGHT + 10)), _WIDTH, _HEIGHT);
		}
        ballUserControl = gameObject.GetComponent<BallUserControl>();
        weaponController = gameObject.GetComponent<WeaponController>();
	}

	void Update()
	{
		if (isLocalPlayer) {
			if (CrossPlatformInputManager.GetButtonUp ("Cancel")) {
				isActive = !isActive;
			}

			Cursor.visible = isActive;
            ballUserControl.isControllable = !isActive;
            weaponController.g_b_IsControllable = !isActive;
        }
	}

	[Command(channel = 0)]
	void CmdChangeClass()
	{
		gameObject.GetComponent<HP> ().p_b_IsDead = true;
	}

	void OnGUI()
	{
		if (isLocalPlayer && isActive) {
			switch (_Language) {
			case Language.English:
				if (GUI.Button (_Button [0], "Continue")) {
					isActive = false;
				}
				break;
			case Language.Russian:
				if (GUI.Button (_Button [0], "Продолжить")) {
					isActive = false;
				}
				break;
			}

			switch (_Language) {
			case Language.English:
				if (GUI.Button (_Button [1], "Change class")) {
					CmdChangeClass ();
				}
				break;
			case Language.Russian:
				if (GUI.Button (_Button [1], "Сменить класс")) {
					CmdChangeClass ();
				}
				break;
			}

			switch (_Language) {
			case Language.English:
				if (GUI.Button (_Button [2], "Disconnect")) {
					if (isServer) {
						manager.StopHost ();
					} else {
						manager.StopClient ();
					}
				}
				break;
			case Language.Russian:
				if (GUI.Button (_Button [2], "Отключиться")) {
					if (isServer) {
						manager.StopHost ();
					} else {
						manager.StopClient ();
					}
				}
				break;
			}
		}
	}
}
