﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

public class StartClient : NetworkBehaviour {

	const float _WIDTH = 250, _HEIGHT = 50;
	public string nickname = "Player";
    Player p_scr_Player;
    // Use this for initialization
    Camera cam;
	void Start () {
			for (int i = 0; i < _Button.GetLength(0); i++)
			{
				_Button[i] = new Rect(Screen.width / 2 - _WIDTH / 2, 25 + (i * (_HEIGHT + 10)), _WIDTH, _HEIGHT);
			}
        rocketeer = Resources.Load(@"Players\Rocketeer") as GameObject;
        demoman = Resources.Load(@"Players\Demoman") as GameObject;
        sniper = Resources.Load(@"Players\Sniper") as GameObject;
        gunner = Resources.Load(@"Players\Gunner") as GameObject;
        cam = gameObject.GetComponent<Camera>();

        if (isServer)
        {
            p_scr_Player = gameObject.GetComponent<Player>();
            if (p_scr_Player.g_g_Guid == Guid.Empty)
            {

                p_scr_Player.g_g_Guid = Guid.NewGuid();
            }
        }
    }

	void Update()
	{
        if (isLocalPlayer)
        {
            if (Cursor.visible == false)
                Cursor.visible = true;
            cam.enabled = true;
        }
	}

	Rect[] _Button = new Rect[5];
	int unit;
	GameObject rocketeer, demoman, sniper, gunner;
	void OnGUI()
	{
		if (isLocalPlayer) {
			int last = unit;
			unit = GUI.SelectionGrid (_Button [0], unit, new string[]{ "Rocketeer", "Demoman", "Sniper", "Gunner" }, 4);
			if (GUI.Button(_Button[1], "OK")) {
				CmdSpawn (unit);
			}
		}
	}

	[Command(channel = 0)]
	void CmdSpawn(int unit)
	{
		GameObject p_go_Obj = null;
		if (unit == 0)
		{
			p_go_Obj = (GameObject) Instantiate (rocketeer);
		}
		if (unit == 1) {
			p_go_Obj = (GameObject) Instantiate (demoman);
		}
		if (unit == 2) {
			p_go_Obj = (GameObject) Instantiate (sniper);
		}
		if (unit == 3) {
			p_go_Obj = (GameObject) Instantiate (gunner);
		}
        GameObject[] p_go_StartPoses = GameObject.FindGameObjectsWithTag("SpawnPos");
        System.Random p_r_Rand = new System.Random();
        p_go_Obj.transform.position = p_go_StartPoses[p_r_Rand.Next(p_go_StartPoses.GetLength(0) - 1)].transform.position;
        NetworkServer.Spawn(p_go_Obj);
        NetworkServer.ReplacePlayerForConnection(connectionToClient, p_go_Obj, playerControllerId);
        p_go_Obj.GetComponent<Player>().g_g_Guid = p_scr_Player.g_g_Guid;
        NetworkServer.Destroy (gameObject);
	}
}
