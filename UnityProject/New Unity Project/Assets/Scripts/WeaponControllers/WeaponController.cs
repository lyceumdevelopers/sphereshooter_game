﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class WeaponController : NetworkBehaviour {
    public float p_f_Scatter = 0.2F;
    protected float p_f_NetDamp = 60,
                p_f_PosDamping = 4,
                p_f_LazorReloadedTime = 0.4F,
                p_f_MaxLazorFlyLength = 500,
                p_f_SizeDamping = 4,
                p_f_SwordSpeed = 360,
                
                p_f_LastShootTime;
    protected Vector3 _InPos = Vector3.zero,
                    _OutPos = new Vector3(-0.377F, -0.24F, 0.779F),
                    _MinSize = new Vector3(0.1F, 0.1F, 0.1F);
    protected Rigidbody p_r_Rigidbody;
    protected NetworkIdentity p_n_Identity;

    public bool g_b_IsControllable,
                g_b_AIIsFire,
                g_b_IsAI;
    public Transform g_t_Head,
        g_t_Downside,
        g_t_Lazor,
        g_t_Sword,
        g_t_Camera;
    Vector3 p_v3_LazorMaxSize,
            p_v3_SwordMaxSize;
    bool p_b_IsLazorReloaded = true;

    Rect p_r_LazorCrosshairRect;
    public Texture LazorCrosshair;
    public Texture LazorTexture, SwordTexture, GeneralTexture;
    AudioSource p_a_LazorAudioSource;

    // Use this for initialization
    public void BasicStart () {
        Transform[] childrenObjects = gameObject.GetComponentsInChildren<Transform>();
        foreach (Transform children in childrenObjects)
        {
            if (children.name == "Head")
            {
                g_t_Head = children;
            }
            else if (children.name == "Downside")
            {
                g_t_Downside = children;
            }
            else if (children.name == "Lazor")
            {
                g_t_Lazor = children;
            }
            else if (children.name == "Sword")
            {
                g_t_Sword = children;
            }
        }

        g_t_Camera = g_t_Head.GetComponentInChildren<Camera>().transform;

        if (isLocalPlayer)
        {
            foreach (Camera cm in Camera.allCameras)
            {
                cm.GetComponent<Camera>().enabled = false;
				cm.GetComponent<AudioListener>().enabled = false;
            }
            g_t_Camera.GetComponent<Camera>().enabled = true;
			g_t_Camera.GetComponent<AudioListener>().enabled = true;
        }

        p_r_Rigidbody = g_t_Downside.GetComponent<Rigidbody>();
        p_n_Identity = gameObject.GetComponent<NetworkIdentity>();

        p_v3_LazorMaxSize = g_t_Lazor.localScale;
        p_v3_SwordMaxSize = g_t_Sword.localScale;



        float p_f_Pixel = Screen.width / 15;
        p_r_LazorCrosshairRect = new Rect(Screen.width / 2 - p_f_Pixel / 2, Screen.height / 2 - p_f_Pixel / 2, p_f_Pixel, p_f_Pixel);
        p_a_LazorAudioSource = g_t_Lazor.GetComponent<AudioSource>();

        p_f_Pixel = Screen.height / 30;
        p_r_WeaponRect = new Rect(p_f_Pixel, Screen.height - p_f_Pixel * 8.5F, p_f_Pixel * 10, p_f_Pixel * 2);

        g_b_IsAI = gameObject.tag == "AI";
    }

    public enum Weapon { Sword, Lazor, General }
    public Weapon g_e_Weapon = Weapon.General;

    public void BasicUpdate()
    {
        switch (g_e_Weapon)
        {
            case Weapon.Sword:
                SwordMaximize();

                Minimize(g_t_Lazor);
                break;
            case Weapon.Lazor:
                Maximize(g_t_Lazor, p_v3_LazorMaxSize);

                SwordMinimize();
                break;
            case Weapon.General:

                SwordMinimize();
                Minimize(g_t_Lazor);
                break;
            default:
                break;
        }

        if ((isLocalPlayer) || (isServer && g_b_IsAI))
        {
            if (!g_b_IsAI)
            g_t_Camera.gameObject.SetActive(true);
            switch (g_e_Weapon)
            {
                case Weapon.Sword:
                    SwordMaximize();

                    Minimize(g_t_Lazor);

                    SwordFunctional();
                    break;
                case Weapon.Lazor:
                    Maximize(g_t_Lazor, p_v3_LazorMaxSize);

                    SwordMinimize();

                    LazorFunctional();
                    break;
                default:
                    break;
            }

            if (!g_b_IsAI && g_b_IsControllable)
            {
                if (Input.GetAxis("First") > 0)
                {
                    g_e_Weapon = Weapon.Sword;
                    p_f_LastShootTime = Time.time;
                }
                else if (Input.GetAxis("Second") > 0)
                {
                    g_e_Weapon = Weapon.General;
                    p_f_LastShootTime = Time.time;
                }
                else if (Input.GetAxis("Third") > 0)
                {
                    g_e_Weapon = Weapon.Lazor;
                    p_f_LastShootTime = Time.time;
                }
            }

            BasicReloadingTest();

            if (!isServer)
            {
                CmdUpdateUnit(g_t_Head.transform.rotation, g_e_Weapon);
            }
            else
            {
                RpcUpdateUnit(g_t_Head.transform.rotation, g_e_Weapon);
            }
        }

        g_t_Sword.position = g_t_Downside.position;
    }

    Rect p_r_WeaponRect;
    public void BasicOnGUI()
    {
        if (this.isLocalPlayer)
        {
            switch (g_e_Weapon)
            {
                case Weapon.Lazor:
                    GUI.DrawTexture(p_r_LazorCrosshairRect, LazorCrosshair);
                    GUI.DrawTexture(p_r_WeaponRect, LazorTexture);
                    break;
                case Weapon.Sword:
                    GUI.DrawTexture(p_r_WeaponRect, SwordTexture);
                    break;
                case Weapon.General:
                    GUI.DrawTexture(p_r_WeaponRect, GeneralTexture);
                    break;
                default:
                    break;
            }
        }
    }

    public void Maximize(Transform weapon, Vector3 maxSize)
    {
        if (Vector3.Distance(weapon.localPosition, _OutPos) < 0.5F)
        {
            weapon.localScale = Vector3.Slerp(weapon.localScale, maxSize, Time.deltaTime * p_f_SizeDamping);
        }
        else
        {
            weapon.localPosition = Vector3.Slerp(weapon.localPosition, _OutPos, Time.deltaTime * p_f_PosDamping);
        }
    }

    public void Minimize(Transform weapon)
    {
        if (Vector3.Distance(weapon.localScale, Vector3.one) < 0.5F)
        {
            weapon.localPosition = Vector3.Slerp(weapon.localPosition, Vector3.zero, Time.deltaTime * p_f_PosDamping);
        }
        else
        {
            weapon.localScale = Vector3.Slerp(weapon.localScale, Vector3.zero, Time.deltaTime * p_f_SizeDamping);
        }
    }

    void SwordMaximize()
    {
        g_t_Sword.localScale = Vector3.Slerp(g_t_Sword.localScale, p_v3_SwordMaxSize, Time.deltaTime * p_f_SizeDamping);
    }

    void SwordMinimize()
    {
        g_t_Sword.localScale = Vector3.Slerp(g_t_Sword.localScale, Vector3.zero, Time.deltaTime * p_f_SizeDamping);
    }

    public void BasicReloadingTest()
    {
        switch (g_e_Weapon)
        {
            case Weapon.Lazor:
                if (Time.time - p_f_LastShootTime > p_f_LazorReloadedTime)
                {
                    p_b_IsLazorReloaded = true;
                }
                break;
            default:
                break;
        }
    }

    [ClientRpc(channel = 0)]
    void RpcUpdateUnitPosition(Vector3 position, Vector3 angularVelocity, Vector3 velocity)
    {
        if (!isServer && !isLocalPlayer && p_r_Rigidbody != null)
        {
            g_t_Downside.transform.position = Vector3.Lerp(g_t_Downside.transform.position, position, Time.deltaTime * p_f_NetDamp);
            p_r_Rigidbody.angularVelocity = Vector3.Lerp(p_r_Rigidbody.angularVelocity, angularVelocity, Time.deltaTime * p_f_NetDamp);
            p_r_Rigidbody.velocity = Vector3.Lerp(p_r_Rigidbody.velocity, velocity, Time.deltaTime * p_f_NetDamp);
        }
    }

    [Command(channel = 0)]
    void CmdUpdateUnitPosition(Vector3 position, Vector3 angularVelocity, Vector3 velocity)
    {
        if (!isLocalPlayer)
        {
            g_t_Downside.transform.position = Vector3.Lerp(g_t_Downside.transform.position, position, Time.deltaTime * p_f_NetDamp);
            p_r_Rigidbody.angularVelocity = Vector3.Lerp(p_r_Rigidbody.angularVelocity, angularVelocity, Time.deltaTime * p_f_NetDamp);
            p_r_Rigidbody.velocity = Vector3.Lerp(p_r_Rigidbody.velocity, velocity, Time.deltaTime * p_f_NetDamp);
        }
    }

    [ClientRpc(channel = 0)]
    void RpcUpdateUnit(Quaternion rotation, Weapon _weapon)
    {
        if (!isServer && !isLocalPlayer)
        {
            g_t_Head.transform.rotation = Quaternion.Lerp(g_t_Head.transform.rotation, rotation, Time.deltaTime);
            g_e_Weapon = _weapon;
        }
    }

    [Command(channel = 0)]
    void CmdUpdateUnit(Quaternion rotation, Weapon _weapon)
    {
        if (!isLocalPlayer)
        {
            g_t_Head.transform.rotation = Quaternion.Lerp(g_t_Head.transform.rotation, rotation, Time.deltaTime);
            g_e_Weapon = _weapon;
            RpcUpdateUnit(rotation, _weapon);
        }
    }

    [Command(channel = 0)]
    void CmdDoLazorOrSwordShot(Vector3 shot_pos, Vector3 direction, Weapon weapon, string ID)
    {
        switch (weapon)
        {
            case Weapon.Lazor:
                GameObject lazor = Instantiate(Resources.Load("Lazor")) as GameObject;
                lazor.transform.position = shot_pos;
                lazor.GetComponent<LazorFly>()._Direction = direction;
                lazor.GetComponent<LazorFly>().g_g_KillerGuid = gameObject.GetComponent<Player>().g_g_Guid;
                NetworkServer.Spawn(lazor as GameObject);
                p_a_LazorAudioSource.Play();
                RpcPlayLazorAudio();
                break;
            default:
                break;
        }
    }

    [ClientRpc(channel = 0)]
    void RpcPlayLazorAudio()
    {
        if (this.isClient && !this.isLocalPlayer)
        {
            p_a_LazorAudioSource.Play();
        }
    }

    public void BasicFixedUpdate()
    {
        if (this.isLocalPlayer && !this.isServer)
        {
            CmdUpdateUnitPosition(g_t_Downside.transform.position, p_r_Rigidbody.angularVelocity, p_r_Rigidbody.velocity);
        }
        else if (this.isServer)
        {
            RpcUpdateUnitPosition(g_t_Downside.transform.position, p_r_Rigidbody.angularVelocity, p_r_Rigidbody.velocity);
        }
    }

    void SwordFunctional()
    {
        if (g_b_IsControllable)
        if (Input.GetAxis("Fire1") > 0)
        {
            g_t_Sword.RotateAround(g_t_Downside.transform.position, Vector3.up, Time.deltaTime * p_f_SwordSpeed);
        }
    }

    void LazorFunctional()
    {
        if (g_b_IsControllable && p_b_IsLazorReloaded)
        if ((Input.GetAxis("Fire1") > 0 && !g_b_IsAI) || (g_b_IsAI && g_b_AIIsFire))
        {
            p_b_IsLazorReloaded = false;
            p_f_LastShootTime = Time.time;

            Vector3 startPos = g_t_Lazor.transform.position + g_t_Lazor.forward * (g_t_Lazor.lossyScale.z + g_t_Lazor.transform.lossyScale.y);
            Vector3 direction;
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(g_t_Head.transform.position, g_t_Head.transform.forward, out hit, p_f_MaxLazorFlyLength))
            {
                Debug.Log(hit.collider);
                direction = (hit.point - startPos).normalized;
            }
            else
            {
                direction = g_t_Lazor.forward;
            }

            string ID = p_n_Identity.netId.ToString();
            p_a_LazorAudioSource.Play();
            if (!g_b_IsAI)
                CmdDoLazorOrSwordShot(startPos, direction, Weapon.Lazor, ID);
            else
                CmdDoLazorOrSwordShot(startPos, direction + (Vector3.up * Random.Range(-p_f_Scatter, p_f_Scatter)) + (Vector3.right * Random.Range(-p_f_Scatter, p_f_Scatter)), Weapon.Lazor, ID);
        }
    }
}
