﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using UnityEngine.Networking;

public class GunnerWeaponController : WeaponController
{
    const float _Damage = 30;
    public Vector3 _GunPos;

    public float _GunReloadedTime = 0.3F;

    public Texture _GunCrosshair,
                   _LazorCrosshair;

    Transform p_t_Gun;
    Vector3 p_v3_GunMaxSize;
    AudioSource p_a_GunAudioSource;

    // Use this for initialization
    void Start()
    {
        BasicStart();

        Transform[] childrenObjects = gameObject.GetComponentsInChildren<Transform>();

        foreach (Transform children in childrenObjects)
        {
            if (children.name == "Gun")
            {
                p_t_Gun = children;
                break;
            }
        }

        p_v3_GunMaxSize = p_t_Gun.localScale;

        p_a_GunAudioSource = p_t_Gun.GetComponent<AudioSource>();

        float p_f_Pixel = Screen.width / 15;
        p_r_GunRect = new Rect(Screen.width / 2 - p_f_Pixel / 2, Screen.height / 2 - p_f_Pixel / 2, p_f_Pixel, p_f_Pixel);
    }

    [Command(channel = 0)]
    void CmdDoShot(Vector3 shot_pos, Vector3 direction, Weapon weapon)
    {
        switch (weapon)
        {
            case Weapon.General:
                GameObject bullet = Instantiate(Resources.Load("GunBullet")) as GameObject;
                bullet.transform.position = shot_pos;
                bullet.GetComponent<LazorFly>()._Direction = direction;
                bullet.GetComponent<LazorFly>().g_g_KillerGuid = gameObject.GetComponent<Player>().g_g_Guid;
                bullet.GetComponent<LazorFly>()._Damage = _Damage;
                NetworkServer.Spawn(bullet as GameObject);
                break;
        }
    }

	void FixedUpdate()
	{   
		BasicFixedUpdate ();
	}

    // Update is called once per frame
    void Update()
    {
        BasicUpdate();

		switch (g_e_Weapon) {
		case Weapon.General:
			Maximize (p_t_Gun, p_v3_GunMaxSize);
			break;
		case Weapon.Lazor:
			Minimize (p_t_Gun);
			break;
		case Weapon.Sword:
			Minimize (p_t_Gun);
			break;
		}

		if ((isLocalPlayer && !g_b_IsAI) || (g_b_IsAI && isServer)) {
			ReloadingTest ();
			if (g_e_Weapon == Weapon.General)
				GunFunctional ();
		}
    }

    void ReloadingTest()
    {
        switch (g_e_Weapon)
        {
            case Weapon.General:
                if (Time.time - p_f_LastShootTime > _GunReloadedTime)
                {
                    p_b_IsGunReloaded = true;
                }
                break;
            default:
                break;
        }
    }

    Rect p_r_GunRect;

    void OnGUI()
    {
        BasicOnGUI();
        if (this.isLocalPlayer)
        {
            switch (g_e_Weapon)
            {
                case Weapon.General:
                    GUI.DrawTexture(p_r_GunRect, _GunCrosshair);
                    break;
                default:
                    break;
            }
        }
    }

    bool p_b_IsGunReloaded = true;
    void GunFunctional()
    {
        if (g_b_IsControllable && p_b_IsGunReloaded)
        if ((Input.GetAxis("Fire1") > 0 && !g_b_IsAI) || (g_b_IsAI && g_b_AIIsFire))
        {
            p_b_IsGunReloaded = false;
            p_f_LastShootTime = Time.time;

            Vector3 startPos = p_t_Gun.transform.position + p_t_Gun.forward * (p_t_Gun.lossyScale.z + p_t_Gun.transform.lossyScale.y);
            Vector3 direction;
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(g_t_Head.transform.position, g_t_Head.transform.forward, out hit, p_f_MaxLazorFlyLength))
            {
                Debug.Log(hit.collider);
                direction = (hit.point - startPos).normalized;
            }
            else
            {
                direction = p_t_Gun.forward;
            }

            string ID = p_n_Identity.netId.ToString();
            p_a_GunAudioSource.Play();
            if (!g_b_IsAI) 
                CmdDoShot(startPos, direction, Weapon.General);
            else
                CmdDoShot(startPos, direction + (Vector3.up * Random.Range(-p_f_Scatter, p_f_Scatter)) + (Vector3.right * Random.Range(-p_f_Scatter, p_f_Scatter)), Weapon.General);
        }
    }
}
