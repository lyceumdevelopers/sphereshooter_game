﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;

public class DemomanWeaponController : WeaponController {
    public Texture _MineCrosshair;
    public ArrayList g_a_Mines = new ArrayList();
    public bool g_b_AIIsMineActive = false;

    Transform p_t_MineLauncher;
    Vector3 p_v3_MinePos = new Vector3(0, -0.26F, 0.77F),
            p_v3_MineMaxSize;

    float p_f_MineReloadedTime = 1,
          p_f_MaxMineForce = 20;

    bool p_b_isMineReloaded = true;

    // Use this for initialization

    void Start()
    {
        BasicStart();

		Transform[] childrenObjects = g_t_Head.GetComponentsInChildren<Transform>();

        foreach (Transform children in childrenObjects)
        {
            if (children.name == "MineLauncher")
            {
                p_t_MineLauncher = children;
                break;
            }
        }

        float pixel = Screen.width / 15;
        mineLauncherRect = new Rect(Screen.width / 2 - pixel / 2, Screen.height / 2 - pixel / 2, pixel, pixel);
        p_v3_MineMaxSize = p_t_MineLauncher.localScale;
    }

	void FixedUpdate()
	{   
		BasicFixedUpdate ();
	}

	[Command(channel = 0)]
	void CmdDoShot(Vector3 shot_pos, Vector3 direction, Weapon weapon, string ID)
	{
		switch (weapon)
		{
		case Weapon.General:
			GameObject mine = Instantiate (Resources.Load ("Mine")) as GameObject;
			mine.transform.position = shot_pos;
			mine.GetComponent<Rigidbody>().AddForce((g_t_Head.forward * 2 + g_t_Head.up).normalized * p_f_MaxMineForce);
			NetworkServer.Spawn (mine as GameObject);
			g_a_Mines.Add (mine);
			break;
		}
	}

	[Command(channel = 0)]
	void CmdMineActive()
	{
		foreach (GameObject mine in g_a_Mines)
		{
			if (mine != null)
				mine.GetComponent<Mine>()._IsPressed = true;
		}
	}

    void Update()
    {
        BasicUpdate();

        if (g_b_IsControllable) {
			switch (g_e_Weapon) {
			case Weapon.General:
				MineMaximize ();
				break;
			case Weapon.Lazor:
				Minimize (p_t_MineLauncher);
				break;
			case Weapon.Sword:
				Minimize (p_t_MineLauncher);
				break;
			}

			if ((isLocalPlayer && !g_b_IsAI) || (g_b_IsAI && isServer)) {
				ReloadingTest ();
				if (g_e_Weapon == Weapon.General)
					MineFunctional ();
			}
		}
    }

    void MineMaximize()
    {
        if ((p_t_MineLauncher.localPosition - _OutPos).magnitude < 0.5F)
        {
            p_t_MineLauncher.localScale = Vector3.Slerp(p_t_MineLauncher.localScale, p_v3_MineMaxSize, Time.deltaTime * p_f_SizeDamping);
        }
        else
        {
            p_t_MineLauncher.localPosition = Vector3.Slerp(p_t_MineLauncher.localPosition, p_v3_MinePos, Time.deltaTime * p_f_PosDamping);
        }
    }

    void ReloadingTest()
    {
        switch (g_e_Weapon)
        {
            case Weapon.General:
                if (Time.time - p_f_LastShootTime > p_f_MineReloadedTime)
                {
                    p_b_isMineReloaded = true;
                }
                break;
            default:
                break;
        }
    }

    Rect mineLauncherRect;

    void OnGUI()
    {
        BasicOnGUI();
		if (this.isLocalPlayer && g_b_IsControllable) {
			switch (g_e_Weapon) {
			case Weapon.General:
				GUI.DrawTexture (mineLauncherRect, _MineCrosshair);
				break;
			default:
				break;
			}
		}
    }

    void MineFunctional()
    {
        if (g_b_IsControllable)
            if ((Input.GetAxis("Fire1") > 0 && p_b_isMineReloaded && !g_b_IsAI) || (g_b_IsAI && g_b_AIIsFire))
        {
            g_b_AIIsFire = false;
            p_b_isMineReloaded = false;
            p_f_LastShootTime = Time.time;

			CmdDoShot (p_t_MineLauncher.transform.position, Vector3.zero, Weapon.General, p_n_Identity.netId.ToString());
        }
        if (g_b_IsControllable)
            if ((Input.GetAxis("Fire2") > 0 && p_b_isMineReloaded && !g_b_IsAI) || (g_b_IsAI && g_b_AIIsMineActive))
        {
            g_b_AIIsMineActive = false;
			CmdMineActive ();
        }
    }
}
