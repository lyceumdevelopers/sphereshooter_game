﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using UnityEngine.Networking;
using UnityStandardAssets.Vehicles.Ball;

public class SniperWeaponController : WeaponController {

    public GameObject g_go_SwordParticle;

    public float g_f_RifleReloadedTime = 1,
                 g_f_MaxBulletFlyLength = 1000,
                 g_f_MinFieldOfRifleView = 5,
                 g_f_MaxFieldOfRifleView = 30,
                 g_f_RifleDamping = 10,
                 g_f_MaxInvisibilityPeriod = 30,
                 g_f_InvisibilityPeriodDown = 4,
                 g_f_InvisibilityPeriodUp = 2,
                 g_f_InvisibilityPeriod;

	public Texture g_t_RifleCrosshair,
                g_t_InvisibleBoxTexture,
                g_t_InvisibleStatusTexture;

    public Material g_m_InvisibilityMaterial;

    public bool g_b_IsInvisible;

    // Use this for initialization
    Vector3 p_v3_RifleMaxSize;
    float p_f_NormalFOV,
        p_f_CurrentFOV,
        p_f_NormalXSpeed,
        p_f_NormalYSpeed;
    VignetteAndChromaticAberration p_ef_Effect;
    MeshRenderer p_m_DownsideRenderer,
        p_m_HeadRenderer,
        p_m_RifleRenderer,
        p_m_LazorRenderer,
        p_m_SwordRenderer;
    Material p_m_StandartDownsideMaterial,
        p_m_StandartHeadMaterial,
        p_m_StandartRifleMaterial,
        p_m_StandartLazorMaterial,
        p_m_StandartSwordMaterial;
	Transform p_t_Rifle;
	Camera p_c_Cam;
	AudioSource p_a_RifleAudioSource;
    BallUserControl p_scr_BallUserControl;

    void Start()
    {
		BasicStart ();

		Transform[] childrenObjects = g_t_Head.GetComponentsInChildren<Transform>();
		foreach (Transform children in childrenObjects)
		{
			if (children.name == "Rifle")
			{
				p_t_Rifle = children;
				break;
			}
		}

		p_c_Cam = g_t_Camera.GetComponent<Camera> ();
        p_v3_RifleMaxSize = p_t_Rifle.transform.localScale;
		p_f_NormalFOV = p_c_Cam.fieldOfView;
        p_f_CurrentFOV = g_f_MinFieldOfRifleView;
        p_ef_Effect = g_t_Camera.GetComponent<VignetteAndChromaticAberration>();

        p_m_DownsideRenderer = g_t_Downside.GetComponent<MeshRenderer>();
        p_m_HeadRenderer = g_t_Head.GetComponent<MeshRenderer>();
        p_m_RifleRenderer = p_t_Rifle.GetComponent<MeshRenderer>();
        p_m_SwordRenderer = g_t_Sword.GetComponent<MeshRenderer>();
        p_m_LazorRenderer = g_t_Lazor.GetComponent<MeshRenderer>();

        p_m_StandartDownsideMaterial = p_m_DownsideRenderer.material;
        p_m_StandartRifleMaterial = p_m_RifleRenderer.material;
        p_m_StandartLazorMaterial = p_m_LazorRenderer.material;
        p_m_StandartSwordMaterial = p_m_SwordRenderer.material;
        p_m_StandartHeadMaterial = p_m_HeadRenderer.material;

        g_f_InvisibilityPeriod = g_f_MaxInvisibilityPeriod;

		float pixel = Screen.width / 15;
		_rifleRect = new Rect(Screen.width / 2 - pixel / 2, Screen.height / 2 - pixel / 2, pixel, pixel);
        pixel = Screen.height / 30;
		_invisibilityBoxRect = new Rect(pixel, Screen.height - pixel * 6, pixel * 10, pixel);
        _invisibilityStatusRect = _invisibilityBoxRect;

		p_a_RifleAudioSource = p_t_Rifle.GetComponent<AudioSource> ();
        p_scr_BallUserControl = gameObject.GetComponent<BallUserControl>();
        p_f_NormalXSpeed = p_scr_BallUserControl._XSpeed;
        p_f_NormalYSpeed = p_scr_BallUserControl._YSpeed;
    }

    bool _isRifleReloaded = true;

	[ClientRpc(channel = 0)]
	void RpcMakeInvisibility(bool invisibilityStatus)
	{
		if (this.isClient && !this.isLocalPlayer)
		{
			if (invisibilityStatus) {
				Hide ();
			} else {
				Show ();
			}
		}
	}

	[Command(channel = 0)]
	void CmdMakeInvisibility(bool invisibilityStatus)
	{
		RpcMakeInvisibility (invisibilityStatus);
		if (!this.isLocalPlayer)
		{
			if (invisibilityStatus) {
				Hide ();
			} else {
				Show ();
			}
		}
	}

	[ClientRpc(channel = 0)]
	void RpcRifleShotAudioPlay()
	{
		if (this.isClient && !this.isLocalPlayer)
		{
			p_a_RifleAudioSource.Play ();
		}
	}

	[Command(channel = 0)]
	void CmdDoShot(Vector3 shot_pos, Vector3 direction, Weapon weapon, string ID)
	{
		switch (weapon)
		{
		case Weapon.General:
			GameObject bullet = Instantiate (Resources.Load ("Bullet")) as GameObject;
			bullet.transform.position = shot_pos;
			bullet.GetComponent<LazorFly> ()._Direction = direction;
			bullet.GetComponent<LazorFly> ().g_g_KillerGuid = gameObject.GetComponent<Player>().g_g_Guid;
			RpcRifleShotAudioPlay ();
			NetworkServer.Spawn (bullet as GameObject);
			break;
		default:
			break;
		}
	}

    // Update is called once per frame
    void Update()
    {
		BasicUpdate ();
		switch (g_e_Weapon) {
			case Weapon.Sword:
			Minimize (p_t_Rifle);
				break;
		case Weapon.General:
			Maximize (p_t_Rifle, p_v3_RifleMaxSize);
				break;
		case Weapon.Lazor:
			Minimize (p_t_Rifle);
				break;
		}

		if ((isLocalPlayer) || (g_b_IsAI && isServer)) {
			switch (g_e_Weapon) {
			case Weapon.General:
				RifleFunctional ();
				break;
			case Weapon.Lazor:
				ZoomTest (false);
				break;
			case Weapon.Sword:
				ZoomTest (false);
				break;
			}

            if (g_b_IsControllable)
                if (!g_b_IsAI)
			g_b_IsInvisible = Input.GetAxis ("Fire2") > 0;

			InvisibilityFunctional ();
			ReloadingTest ();
		}

		g_t_Sword.position = g_t_Downside.position;
    }

    bool invisibilityActive;

    float _lastShootTime;

	void FixedUpdate()
	{   
		BasicFixedUpdate ();
	}

    void ReloadingTest()
    {
        switch (g_e_Weapon)
        {
			case Weapon.General:
                if (Time.time - _lastShootTime > g_f_RifleReloadedTime)
                {
                    _isRifleReloaded = true;
                }
                break;
        }
    }

    void InvisibilityFunctional()
    {
        if (g_b_IsInvisible)
        {
            if (g_f_InvisibilityPeriod > 0)
            {
                g_f_InvisibilityPeriod -= g_f_InvisibilityPeriodDown * Time.deltaTime;
                if (!invisibilityActive)
                {
					invisibilityActive = true;
                    Hide();
					CmdMakeInvisibility (true);
                }
            }
            else
            {
                if (invisibilityActive)
                {
                    invisibilityActive = false;
					Show();
					CmdMakeInvisibility (false);
                }
            }
        }
        else
        {
            if (g_f_InvisibilityPeriod < g_f_MaxInvisibilityPeriod)
            g_f_InvisibilityPeriod += g_f_InvisibilityPeriodUp * Time.deltaTime;
            if (invisibilityActive)
            {
                invisibilityActive = false;
				Show();
				CmdMakeInvisibility (false);
            }
        }

        _invisibilityStatusRect.width = (g_f_InvisibilityPeriod * _invisibilityBoxRect.width) / g_f_MaxInvisibilityPeriod;
    }

    void Show()
    {
        p_m_DownsideRenderer.material = p_m_StandartDownsideMaterial;
        p_m_HeadRenderer.material = p_m_StandartHeadMaterial;
        p_m_RifleRenderer.material = p_m_StandartRifleMaterial;
        p_m_SwordRenderer.material = p_m_StandartSwordMaterial;
        p_m_LazorRenderer.material = p_m_StandartLazorMaterial;
    }

    void Hide()
    {
        p_m_DownsideRenderer.material = g_m_InvisibilityMaterial;
        p_m_HeadRenderer.material = g_m_InvisibilityMaterial;
        p_m_RifleRenderer.material = g_m_InvisibilityMaterial;
        p_m_SwordRenderer.material = g_m_InvisibilityMaterial;
        p_m_LazorRenderer.material = g_m_InvisibilityMaterial;
    }

    Rect _rifleRect, _lazorRect, _invisibilityBoxRect, _invisibilityStatusRect;

    void OnGUI()
    {
		BasicOnGUI ();
		if (isLocalPlayer) {
			switch (g_e_Weapon) {
			case Weapon.General:
				if (isZoomed)
					GUI.DrawTexture (_rifleRect, g_t_RifleCrosshair);
				break;
			}

			GUI.DrawTexture (_invisibilityBoxRect, g_t_InvisibleBoxTexture);
			GUI.DrawTexture (_invisibilityStatusRect, g_t_InvisibleStatusTexture);
            GUI.contentColor = Color.black;
            GUI.Label(_invisibilityBoxRect, "INV: " + (int)g_f_InvisibilityPeriod + "/" + (int)g_f_MaxInvisibilityPeriod);
        }
    }

    bool isZoomed = false, pressed = false;

	void ZoomTest(bool isZoomed)
	{
		if (isZoomed)
		{
			p_f_CurrentFOV -= (Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime) * g_f_RifleDamping * 3 * Mathf.Abs(p_f_CurrentFOV);
			p_f_CurrentFOV = Mathf.Clamp(p_f_CurrentFOV, g_f_MinFieldOfRifleView, g_f_MaxFieldOfRifleView);

			p_c_Cam.fieldOfView = Mathf.Lerp(p_c_Cam.fieldOfView, p_f_CurrentFOV, g_f_RifleDamping * Time.deltaTime);
			p_ef_Effect.intensity = Mathf.Lerp(p_ef_Effect.intensity, 0.65F, g_f_RifleDamping * Time.deltaTime);
			p_ef_Effect.chromaticAberration = Mathf.Lerp(p_ef_Effect.chromaticAberration, 30, g_f_RifleDamping * Time.deltaTime);
		}
		else
		{
			p_c_Cam.fieldOfView = Mathf.Lerp(p_c_Cam.fieldOfView, p_f_NormalFOV, g_f_RifleDamping * Time.deltaTime);
			p_ef_Effect.intensity = Mathf.Lerp(p_ef_Effect.intensity, 0, g_f_RifleDamping * Time.deltaTime);
			p_ef_Effect.chromaticAberration = Mathf.Lerp(p_ef_Effect.chromaticAberration, 0, g_f_RifleDamping * Time.deltaTime);
		}
	}

    void RifleFunctional()
    {
        p_scr_BallUserControl._XSpeed = p_f_NormalXSpeed * (p_c_Cam.fieldOfView / p_f_NormalFOV);
        p_scr_BallUserControl._YSpeed = p_f_NormalYSpeed * (p_c_Cam.fieldOfView / p_f_NormalFOV);

        if (g_b_IsControllable)
            if (Input.GetAxis("Fire3") > 0 && !g_b_IsAI)
        {
            if (!pressed)
            {
                pressed = true;
                isZoomed = !isZoomed;                
            }
        }
        if (g_b_IsControllable)
            if (Input.GetAxis("Fire3") == 0 && !g_b_IsAI)
        {
            pressed = false;
        }

        if (!g_b_IsAI)
		ZoomTest (isZoomed);

        if (g_b_IsControllable)
            if ((Input.GetAxis("Fire1") > 0 && !g_b_IsAI && _isRifleReloaded) || (g_b_IsAI && g_b_AIIsFire & _isRifleReloaded))
        {
            _isRifleReloaded = false;
            _lastShootTime = Time.time;
			Vector3 startPos = p_t_Rifle.transform.position + p_t_Rifle.forward * (p_t_Rifle.lossyScale.z + p_t_Rifle.transform.lossyScale.y);
			Vector3 direction;

            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(g_t_Head.transform.position, g_t_Head.transform.forward, out hit, g_f_MaxBulletFlyLength))
            {
				direction = (hit.point - startPos).normalized;
            }
            else
            {
				direction = p_t_Rifle.transform.forward;
            }
			string ID = p_n_Identity.netId.ToString();
			p_a_RifleAudioSource.Play ();
            if (!g_b_IsAI)
                CmdDoShot(startPos, direction, Weapon.General, ID);
            else
                CmdDoShot(startPos, direction + (Vector3.up * Random.Range(-p_f_Scatter, p_f_Scatter)) + (Vector3.right * Random.Range(-p_f_Scatter, p_f_Scatter)), Weapon.General, ID);
        }
    }
}
