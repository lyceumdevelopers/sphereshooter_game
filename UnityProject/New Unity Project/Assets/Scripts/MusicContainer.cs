﻿using UnityEngine;
using System.Collections;

public class MusicContainer : MonoBehaviour {

    AudioSource[] p_as_AudioSources;
    System.Random p_r_Rand = new System.Random();
    int p_i_Current = 0;
	// Use this for initialization
	void Start () {
        p_as_AudioSources = gameObject.GetComponents<AudioSource>();
        p_i_Current = p_r_Rand.Next(p_as_AudioSources.GetLength(0));
        p_as_AudioSources[p_i_Current].Play();
    }
	
	// Update is called once per frame
	void Update () {
        if (!p_as_AudioSources[p_i_Current].isPlaying)
        {
            p_i_Current = p_r_Rand.Next(p_as_AudioSources.GetLength(0));
            p_as_AudioSources[p_i_Current].Play();
        }
	}
}
