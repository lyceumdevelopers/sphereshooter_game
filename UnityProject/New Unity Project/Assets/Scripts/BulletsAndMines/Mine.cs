﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

public class Mine : NetworkBehaviour {
	[SyncVar]
    public bool _IsActive = true,
                _IsPressed = false;
    public GameObject light;
    public float _Delay = 5, _Damage = 100;
    public Guid g_g_KillerGuid;
    float startTime;
	// Use this for initialization
	float _Radius;
	void Start () {
        startTime = Time.time;
		_Radius = gameObject.GetComponent<SphereCollider> ().radius / 2;
    }
	
	// Update is called once per frame
	void Update () {
		if (isServer) {
			if (_IsPressed && _IsActive && Time.time - startTime > _Delay) {
				CmdBang ();
			}
		}
        if (Time.time - startTime > _Delay)
        {
            light.GetComponent<Light>().color = Color.red;
        }
	}

	[Command(channel = 0)]
    void CmdBang()
    {
		Collider[] targets = Physics.OverlapSphere(transform.position, _Radius);
		foreach (Collider current in targets)
		{
			try
			{
				if (current.gameObject != null)
				{
					if (current.gameObject.tag == "Destroyable"){
						ObjectsDestructible script = current.gameObject.GetComponent<ObjectsDestructible>();
						script._Damage = _Damage;
						script._Radius = _Radius;
						script._isActive = true;
						script._BangPosition = transform.position;
					}
					if (current.gameObject.tag == "Player")
					{
						current.gameObject.GetComponentInParent<HP>().CmdDamage(_Damage / Vector3.Distance(transform.position, current.transform.position), g_g_KillerGuid);
					}
                    if (current.gameObject.tag == "Mine")
                    {
                        current.gameObject.GetComponent<Mine>()._IsPressed = true;
                    }
                }
			}
			catch { }
		}
        GameObject bang = Instantiate(Resources.Load("Bang")) as GameObject;
        bang.transform.position = gameObject.transform.position;
		NetworkServer.Spawn (bang);
		NetworkServer.Destroy(gameObject);
    }

    void OnTriggerEnter(Collider col)
    {
		if (isServer)
        if (col.tag == "Player" && Time.time - startTime > _Delay)
        {
            CmdBang();
        }
    }
}
