﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

public class LazorFly : NetworkBehaviour {
	[SyncVar]
    public Vector3 _Direction;
    public float _FlySpeed = 10, _MaxDistance = 200, _Damage = 30;
	[SyncVar]
	public Guid g_g_KillerGuid;

    Rigidbody _rigidbody;
    Vector3 _startPos;
    // Use this for initialization
    void Start()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody>();
        _startPos = gameObject.transform.position;
        transform.rotation = Quaternion.LookRotation(_Direction);
        transform.rotation = Quaternion.LookRotation(-transform.up);
    }

    void FixedUpdate()
    {
        if (Vector3.Distance(_startPos, transform.position) > _MaxDistance)
        {
            Destroy(gameObject);
        }
        _rigidbody.AddForce(_Direction * _FlySpeed, ForceMode.VelocityChange);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision col)
    {
		if (isServer) {
			if (col.gameObject != null)
			if (col.gameObject.tag == "Player") {
				if (col.gameObject.GetComponentInParent<Player> ().g_g_Guid == g_g_KillerGuid) {
					return;
				} else {
					col.gameObject.GetComponentInParent<HP> ().CmdDamage (_Damage, g_g_KillerGuid);
				}
			}
            if (col.gameObject.tag == "Mine")
            {
                col.gameObject.GetComponent<Mine>()._IsPressed = true;
            }

            GameObject bang = Instantiate (Resources.Load ("LazorFlare")) as GameObject;
			bang.transform.position = gameObject.transform.position;
			NetworkServer.Spawn (bang);
			NetworkServer.Destroy (gameObject);
		}
    }
}
