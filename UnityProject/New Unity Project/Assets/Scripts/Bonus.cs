﻿using UnityEngine;
using System.Collections;

public class Bonus : MonoBehaviour {
	public enum BonusType {Speed,HP,Armor,Points}
	public BonusType _BonusType;
	public bool _Direction = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.Rotate (0,0,60 * Time.deltaTime);
		if (gameObject.transform.position.y >= 3 ) {
			_Direction = false;
		}
		if (gameObject.transform.position.y <= 1.5) {
			_Direction = true;
		}
		if (_Direction) {
			gameObject.transform.Translate (0,0,(float) 1 * Time.deltaTime);
		}
		if (!_Direction) {
			gameObject.transform.Translate (0,0,(float) -1 * Time.deltaTime);
		}
	}
	void OnTriggerEnter(Collider col){
		if (col.tag == "Player") {
			switch (_BonusType) {
			case BonusType.Armor:
				col.gameObject.GetComponentInParent<HP> ()._AMMO = 100;
				Destroy (gameObject);
				break;
			case BonusType.HP:
				col.gameObject.GetComponentInParent<HP> ()._HP = 100;
				Destroy (gameObject);
				break;
			case BonusType.Points:
				col.gameObject.GetComponentInParent<HP> ()._EXP += 100;
				break;
			case BonusType.Speed:
				if (col.gameObject.GetComponentInParent<UnityStandardAssets.Vehicles.Ball.Ball> ().counter > 0) {
					col.gameObject.GetComponentInParent<UnityStandardAssets.Vehicles.Ball.Ball> ().counter += 10;
				} else {
					col.gameObject.GetComponentInParent<UnityStandardAssets.Vehicles.Ball.Ball> ().counter = 30;
				}
				Destroy (gameObject);
				break;
			default:
				break;
			}
		}
	}
}
