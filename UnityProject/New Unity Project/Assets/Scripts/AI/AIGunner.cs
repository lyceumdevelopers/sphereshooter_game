﻿using UnityEngine;
using System.Collections;

public class AIGunner : AI {
    // Use this for initialization
    void Start () {
        BasicStart();
    }

    float lastUpdateTime;
    void Update () {
        if (Time.time - lastUpdateTime > 1)
        {
            lastUpdateTime = Time.time;
            BasicUpdate();
        }
    }
}
