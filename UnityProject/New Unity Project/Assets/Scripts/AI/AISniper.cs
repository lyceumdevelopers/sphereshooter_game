﻿using UnityEngine;
using System.Collections;

public class AISniper : AI {

    SniperWeaponController p_scr_SniperWeaponController;
    int p_i_InvisibleActivationDeltaTime;
    float p_f_LastInvisibleActivationTime = 0;
    bool p_b_IsInvisibleActive = false;
    // Use this for initialization
    void Start () {
        p_scr_SniperWeaponController = gameObject.GetComponent<SniperWeaponController>();
        BasicStart();
    }

    float lastUpdateTime;
    void Update () {
        if (Time.time - lastUpdateTime > 1)
        {
            lastUpdateTime = Time.time;
            BasicUpdate();
        }

        p_scr_SniperWeaponController.g_b_IsInvisible = p_b_IsInvisibleActive;

        if (p_scr_SniperWeaponController.g_f_InvisibilityPeriod > p_scr_SniperWeaponController.g_f_MaxInvisibilityPeriod - 2)
        {
            if (Time.time - p_f_LastInvisibleActivationTime > p_i_InvisibleActivationDeltaTime)
            {
                p_b_IsInvisibleActive = true;
            }
        }
        if (p_scr_SniperWeaponController.g_f_InvisibilityPeriod < 5)
        {
            p_i_InvisibleActivationDeltaTime = p_r_Rand.Next(1, 45);
            p_f_LastInvisibleActivationTime = Time.time;
            p_b_IsInvisibleActive = false;
        }
    }
}
