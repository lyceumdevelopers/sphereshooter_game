﻿using UnityEngine;
using System.Collections;

public class AIDemoman : AI {

    DemomanWeaponController p_scr_DemomanWeaponController;
    float p_f_LastMineActivationTime = 0, p_f_RandomDelta;
    // Use this for initialization
    void Start () {
        p_scr_DemomanWeaponController = gameObject.GetComponent<DemomanWeaponController>();
        BasicStart();
        p_f_RandomDelta = p_r_Rand.Next(5, 30);
    }

    public override void OnFirstMeet()
    {
        p_scr_DemomanWeaponController.g_b_AIIsMineActive = p_r_Rand.Next(1, 3) == 2;
        p_scr_WeaponController.g_e_Weapon = WeaponController.Weapon.Lazor;
        p_e_AIThinkingStatus = AIThinkingStatus.SearchingForShelter;
    }

    public override void OnFreeWalking()
    {
        p_scr_WeaponController.g_e_Weapon = WeaponController.Weapon.General;
        if (Time.time - p_f_LastMineActivationTime > p_f_RandomDelta)
        {
            p_f_LastMineActivationTime = Time.time;
            p_f_RandomDelta = p_r_Rand.Next(5, 30);
            p_scr_WeaponController.g_b_AIIsFire = true;
        }
        else
        {
            p_scr_WeaponController.g_b_AIIsFire = false;
        }
    }

    float lastUpdateTime;
    void Update () {
        if (Time.time - lastUpdateTime > 1)
        {
            lastUpdateTime = Time.time;
            BasicUpdate();
        }
    }
}
