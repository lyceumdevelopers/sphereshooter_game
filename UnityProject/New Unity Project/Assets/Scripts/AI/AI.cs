﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Ball;

public class AI : MonoBehaviour {
    public float g_f_Scatter = 0.1F;
    protected BallUserControl p_scr_BallController;
    protected Transform p_t_Head, p_t_Downside, p_t_Target;
    protected enum AIThinkingStatus { SearchingForShelter, LookingAtEnemy, EnemyFirstMeeting, SearchingForEnemy }
    protected enum AIMovementStatus { Staying, Walking, GoingToShelter }
    protected AIMovementStatus p_e_AIMovementStatus = AIMovementStatus.Walking;
    protected AIThinkingStatus p_e_AIThinkingStatus = AIThinkingStatus.SearchingForEnemy;
    protected Vector3[] p_v3_Shelters;
    protected int p_i_LayerMask = 1 << 8,
        p_i_DirectionIndex = 1,
        p_i_DeltaTime = 5,
        p_i_StayingDeltaTime = 5;
    protected float p_f_LastDirectionChangeTime = 0,
        p_f_StayingStartedTime = 0,
        p_f_GroundViewAgle = 0.3F;
    protected System.Random p_r_Rand = new System.Random();
    protected Rigidbody p_r_Rigidbody;
    protected Vector3[] p_v3_Raycasts = new Vector3[8];
    protected Vector3[] p_v3_Directions = new Vector3[8];
    protected Vector3 p_v3_CurrentDirection, p_v3_CurrentShelter;
    protected bool p_b_FirstMeet = false;
    protected WeaponController p_scr_WeaponController;
    EnemyStatus p_scr_EnemyStatus;
    // Use this for initialization
    public void BasicStart () {
        p_scr_BallController = gameObject.GetComponent<BallUserControl>();
        p_scr_WeaponController = gameObject.GetComponent<WeaponController>();
        p_r_Rigidbody = p_scr_BallController._Downside.GetComponent<Rigidbody>();
        p_t_Head = p_scr_BallController._Head;
        p_t_Downside = p_scr_BallController._Downside;
        p_i_LayerMask = ~p_i_LayerMask;

        GameObject[] p_go_Shelters = GameObject.FindGameObjectsWithTag("Shelter");
        p_v3_Shelters = new Vector3[p_go_Shelters.GetLength(0)];
        for (int i = 0; i < p_v3_Shelters.GetLength(0); i++)
        {
            p_v3_Shelters[i] = p_go_Shelters[i].transform.position;
        }

        p_v3_Directions[0] = Vector3.forward;
        p_v3_Directions[1] = (Vector3.forward + Vector3.right).normalized;
        p_v3_Directions[2] = Vector3.right;
        p_v3_Directions[3] = (-Vector3.forward + Vector3.right).normalized;
        p_v3_Directions[4] = -Vector3.forward;
        p_v3_Directions[5] = (-Vector3.forward - Vector3.right).normalized;
        p_v3_Directions[6] = -Vector3.right;
        p_v3_Directions[7] = (Vector3.forward - Vector3.right).normalized;

        for (int i = 0; i < p_v3_Directions.GetLength(0); i++)
        {
            p_v3_Raycasts[i] = (p_v3_Directions[i] + (Vector3.down * p_f_GroundViewAgle)).normalized;
        }

    }

    public Transform FindTarget(float i_f_Distance)
    {
        GameObject[] p_go_Players = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject p_go_Player in p_go_Players)
        {
            if (p_go_Player.transform.parent != gameObject.transform)
                if ((p_go_Player.transform.position - p_t_Head.position).magnitude < i_f_Distance)
                    if (!Physics.Linecast(p_go_Player.transform.position, p_t_Head.position, p_i_LayerMask))
                    {
                        p_scr_EnemyStatus = p_go_Player.transform.parent.GetComponent<EnemyStatus>();
                        return p_go_Player.transform;
                    }
        }

        return null;
    }

    public virtual void OnFirstMeet()
    {

    }

    public virtual void OnFreeWalking()
    {

    }

    public virtual void Staying()
    {
        p_v3_CurrentDirection = Vector3.zero;
        if (Time.time - p_f_StayingStartedTime > p_i_StayingDeltaTime)
        {
            p_i_StayingDeltaTime = p_r_Rand.Next(1, 8);
            p_f_StayingStartedTime = Time.time;
            p_e_AIMovementStatus = AIMovementStatus.Walking;
        }
    }

    public virtual void SearchingForShelter()
    {
        float p_f_MinDistance = -1;
        p_v3_CurrentShelter = Vector3.zero;
        foreach (Vector3 p_v3_Shelter in p_v3_Shelters)
        {
            if (!Physics.Linecast(p_t_Head.position, p_v3_Shelter, p_i_LayerMask))
            {
                if (p_f_MinDistance != -1)
                {
                    if (Vector3.Distance(p_v3_Shelter, p_t_Head.position) < p_f_MinDistance)
                    {
                        p_v3_CurrentShelter = p_v3_Shelter;
                    }
                }
                else
                {
                    p_v3_CurrentShelter = p_v3_Shelter;
                }
            }
        }
        if (p_v3_CurrentShelter != Vector3.zero)
        {
            p_e_AIMovementStatus = AIMovementStatus.GoingToShelter;
            p_v3_CurrentDirection = p_v3_CurrentShelter - p_t_Head.position;
            p_v3_CurrentDirection.y = 0;
            p_v3_CurrentDirection.Normalize();
        }
        else
        {
            p_e_AIMovementStatus = AIMovementStatus.Walking;
            p_v3_CurrentDirection = Vector3.zero;
        }
    }

    public virtual void GoingToShelter()
    {
        Vector3 p_v3_Direction = p_v3_CurrentShelter - p_t_Downside.position;
        p_v3_Direction.y = 0;
        p_v3_Direction = p_v3_Direction.normalized;
        Vector3 p_v3_Raycast = (p_v3_Direction + Vector3.down).normalized;
        if (!(Physics.Raycast(p_t_Head.transform.position, p_v3_Raycast, 10, p_i_LayerMask) && !Physics.Raycast(p_t_Downside.transform.position, p_v3_Direction, 5, p_i_LayerMask)))
        {
            p_e_AIMovementStatus = AIMovementStatus.Walking;
        }
        if ((p_t_Downside.position - p_v3_CurrentShelter).magnitude < 5)
        {
            p_f_StayingStartedTime = Time.time;
            p_e_AIMovementStatus = AIMovementStatus.Staying;
            p_v3_CurrentDirection = Vector3.zero;
        }
        else
        {
            p_v3_CurrentDirection = p_v3_Direction;
        }
    }

    public virtual void Walking()
    {
        if (Time.time - p_f_LastDirectionChangeTime > p_i_DeltaTime)
        {
            p_i_DeltaTime = p_r_Rand.Next(1, 8);
            p_f_LastDirectionChangeTime = Time.time;
            p_i_DirectionIndex = p_r_Rand.Next(1, 8);
        }
        Vector3 p_v3_FindedDirection = Vector3.zero;
        Vector3 p_v3_Correction = Vector3.zero;
        int p_i_Index = 1;
        float p_f_Length = new Vector2(p_r_Rigidbody.velocity.x, p_r_Rigidbody.velocity.z).magnitude + 3;
        for (int i = 0; i < p_v3_Directions.GetLength(0); i++)
        {
            Debug.DrawRay(p_t_Head.transform.position, p_v3_Raycasts[i] * 21, Color.red);
            Debug.DrawRay(p_t_Head.transform.position, p_v3_Directions[i] * p_f_Length, Color.green);
            if (p_i_Index <= p_i_DirectionIndex)
            {
                if (Physics.Raycast(p_t_Head.transform.position, p_v3_Raycasts[i], 21, p_i_LayerMask))
                {
                    if (!Physics.Raycast(p_t_Downside.transform.position, p_v3_Directions[i], p_f_Length, p_i_LayerMask))
                    {
                        p_i_Index++;
                        p_v3_FindedDirection = p_v3_Directions[i];
                    }
                }
                else
                {
                    p_v3_Correction -= p_v3_Directions[i];
                }
            }
        }

        p_v3_CurrentDirection = (p_v3_FindedDirection + p_v3_Correction).normalized;
    }

    private Vector3 GetDirection()
    {
        switch (p_e_AIMovementStatus)
        {
            case AIMovementStatus.Staying:
                Staying();
                break;
            case AIMovementStatus.GoingToShelter:
                GoingToShelter();
                break;
            case AIMovementStatus.Walking:
                Walking();
                break;
        }

        return p_v3_CurrentDirection;
    }

    // Update is called once per frame
    public void BasicUpdate () {
        Vector3 p_v3_Direction = GetDirection();
        if (p_v3_Direction != Vector3.zero)
        {
            p_scr_BallController.g_v3_AIMoveDirection = p_v3_Direction;
        }
        else
        {
            p_scr_BallController.g_v3_AIMoveDirection = -p_r_Rigidbody.velocity;
        }

        switch (p_e_AIThinkingStatus)
        {
            case AIThinkingStatus.SearchingForEnemy:

                OnFreeWalking();

                p_t_Target = FindTarget(150);
                p_scr_WeaponController.g_e_Weapon = WeaponController.Weapon.General;
                p_scr_WeaponController.g_b_AIIsFire = false;
                if (p_t_Target != null)
                {
                    p_e_AIThinkingStatus = AIThinkingStatus.EnemyFirstMeeting;
                }
                break;
            case AIThinkingStatus.EnemyFirstMeeting:
                OnFirstMeet();
                p_e_AIThinkingStatus = AIThinkingStatus.LookingAtEnemy;
                break;
            case AIThinkingStatus.LookingAtEnemy:
                if (p_t_Target == null)
                {
                    p_e_AIThinkingStatus = AIThinkingStatus.SearchingForEnemy;
                }
                else
                {
                    p_scr_BallController.g_v3_AILookDirection = p_t_Target.position - p_t_Head.transform.position;
                    p_scr_WeaponController.g_b_AIIsFire = true;

                    if (p_scr_EnemyStatus.IsInvisible)
                    {
                        p_scr_WeaponController.p_f_Scatter = g_f_Scatter * 3;
                    }
                    else
                    {
                        p_scr_WeaponController.p_f_Scatter = g_f_Scatter;
                    }

                    if (Physics.Linecast(p_t_Target.position, p_t_Head.position, p_i_LayerMask))
                    {
                        p_e_AIThinkingStatus = AIThinkingStatus.SearchingForEnemy;
                    }
                }
                break;
            case AIThinkingStatus.SearchingForShelter:
                SearchingForShelter();
                p_e_AIThinkingStatus = AIThinkingStatus.LookingAtEnemy;
                break;
        }
    }
}
