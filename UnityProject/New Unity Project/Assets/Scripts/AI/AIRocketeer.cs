﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Ball;

public class AIRocketeer : AI {
    RocketeerWeaponController p_scr_RocketeerWeaponController;
    int p_i_JetpackActivationDeltaTime;
    float p_f_LastJetpackActivationTime = 0;
    bool p_b_IsJetpackActive = false;

    // Use this for initialization
    void Start () {
        p_scr_RocketeerWeaponController = gameObject.GetComponent<RocketeerWeaponController>();
        BasicStart();
        p_i_JetpackActivationDeltaTime = p_r_Rand.Next(1, 30);
    }

    public override void Walking()
    {
        if (Time.time - p_f_LastDirectionChangeTime > p_i_DeltaTime)
        {
            p_i_DeltaTime = p_r_Rand.Next(1, 8);
            p_f_LastDirectionChangeTime = Time.time;
            p_i_DirectionIndex = p_r_Rand.Next(1, 8);
        }
        Vector3 p_v3_FindedDirection = Vector3.zero;
        Vector3 p_v3_Correction = Vector3.zero;
        int p_i_Index = 1;
        for (int i = 0; i < p_v3_Directions.GetLength(0); i++)
        {
            Debug.DrawRay(p_t_Head.transform.position, p_v3_Raycasts[i] * 21, Color.red);
            Debug.DrawRay(p_t_Head.transform.position, p_v3_Directions[i] * 6, Color.green);
            if (p_i_Index <= p_i_DirectionIndex)
            {
                if (!p_b_IsJetpackActive)
                {
                    if (Physics.Raycast(p_t_Head.transform.position, p_v3_Raycasts[i], 21, p_i_LayerMask))
                    {
                        if (!Physics.Raycast(p_t_Downside.transform.position, p_v3_Directions[i], 6, p_i_LayerMask))
                        {
                            p_i_Index++;
                            p_v3_FindedDirection = p_v3_Directions[i];
                        }
                    }
                    else
                    {
                        p_v3_Correction -= p_v3_Directions[i];
                    }
                }
                else
                {
                    Vector3 p_v3_RaycastPos = new Vector3(p_r_Rigidbody.velocity.x, 0, p_r_Rigidbody.velocity.z);
                    if (Physics.Raycast(p_t_Head.transform.position + p_v3_RaycastPos, p_v3_Raycasts[i], 50, p_i_LayerMask))
                    {
                        p_i_Index++;
                        p_v3_FindedDirection = p_v3_Directions[i] + Vector3.up * 1.5F;
                    }
                }
            }
        }

        p_v3_CurrentDirection = (p_v3_FindedDirection + p_v3_Correction).normalized;

        if (p_b_IsJetpackActive)
        {
            p_scr_RocketeerWeaponController.g_v3_AIJetpackDirection = p_v3_CurrentDirection;
        }
    }

    float lastUpdateTime;
    void Update() {
        if (Time.time - lastUpdateTime > 1)
        {
            lastUpdateTime = Time.time;
            BasicUpdate();
        }

        p_scr_RocketeerWeaponController.g_b_AIJetpackStatus = p_b_IsJetpackActive;
        if (p_e_AIMovementStatus == AIMovementStatus.Walking)
        {
            if (p_scr_RocketeerWeaponController.g_f_JetpackTime > p_scr_RocketeerWeaponController.g_f_JetpackMaxTime - 2)
            {
                if (Time.time - p_f_LastJetpackActivationTime > p_i_JetpackActivationDeltaTime)
                {
                    p_b_IsJetpackActive = true;
                }
            }
            if (p_scr_RocketeerWeaponController.g_f_JetpackTime < 5)
            {
                p_i_JetpackActivationDeltaTime = p_r_Rand.Next(1, 30);
                p_f_LastJetpackActivationTime = Time.time;
                p_b_IsJetpackActive = false;
            }
        }
        else
        {
            p_b_IsJetpackActive = false;
        }
    }
}
