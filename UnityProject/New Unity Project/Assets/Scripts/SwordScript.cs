﻿using UnityEngine;
using System.Collections;

public class SwordScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        GameObject flare;
        flare = Instantiate(Resources.Load("LazorFlare")) as GameObject;
        flare.transform.position = other.ClosestPointOnBounds(gameObject.transform.position);
    }
}
