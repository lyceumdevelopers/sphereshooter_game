﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.PathManager;

public class PathManager : MonoBehaviour {

    const int layerMask = ~(1 << 8);

    public Transform Character;
    public float StepHeight = 2,
        CharacterHeight = 5,
        CharacterRadius = 3,
        JumpHeight = 5;

    List<PathElement> pathElements = new List<PathElement>();
    

	void Start () {
        CalculatePathElementsArray();
	}
	
	void Update () {
	
	}

    void CalculatePathElementsArray()
    {
        pathElements.Clear();
        pathElements.Add(CreatePathElement(Character.position));

        foreach (PathElement pathElement in pathElements)
        {
            for (int i = 0; i < pathElement.AdjacentPathElements.GetLength(0); i++)
            {
                if (pathElement.AdjacentPathElements[i] == null)
                {
                    
                }
            }
        }
    }

    private PathElement CreatePathElement(Vector3 position)
    {
        RaycastHit raycastHit;
        Physics.Raycast(position, Vector3.down, out raycastHit);

        Vector3 groundedPoint = raycastHit.point;
        Vector3[] adjacentPoints;
        bool[] isJump;
        GetAdjacent(out adjacentPoints, out isJump, groundedPoint);
        return new PathElement(groundedPoint, adjacentPoints, isJump);
    }

    private void GetAdjacent(out Vector3[] returnedArray, out bool[] isJump, Vector3 groundedPosition)
    {
        returnedArray = new Vector3[PathElement.AdjacentPathElementsCount];
        isJump = new bool[PathElement.AdjacentPathElementsCount];
        Vector3 stepPosition = new Vector3(groundedPosition.x, groundedPosition.y + StepHeight, groundedPosition.z);
        Vector3 headPosition = new Vector3(groundedPosition.x, groundedPosition.y + CharacterHeight, groundedPosition.z);
        Vector3 jumpPosition = new Vector3(groundedPosition.x, groundedPosition.y + JumpHeight, groundedPosition.z);

        for (int i = 0; i < PathElement.AdjacentPathElementsCount; i++)
        {
            RaycastHit raycastHit;
            Vector3 direction = Quaternion.AngleAxis(45 * i, Vector3.up) * Vector3.forward;

            bool isIntersectFromStepPos = Physics.Raycast(stepPosition, direction, CharacterRadius, layerMask),
                isIntersectFromHeadPos = Physics.Raycast(headPosition, direction, CharacterRadius, layerMask),
                isIntersectFromJumpPos = Physics.Raycast(jumpPosition, direction, CharacterRadius, layerMask);

            if (!isIntersectFromStepPos &&
                !isIntersectFromHeadPos)
            {
                Vector3 point = stepPosition + direction * CharacterRadius;
                if (Physics.Raycast(point, Vector3.down, out raycastHit, 100, layerMask))
                {
                    returnedArray[i] = raycastHit.point;
                }
            }
            else if (isIntersectFromStepPos && !isIntersectFromJumpPos)
            {
                Vector3 point = jumpPosition + direction * CharacterRadius;
                if (Physics.Raycast(point, Vector3.down, out raycastHit, 100, layerMask))
                {
                    returnedArray[i] = raycastHit.point;
                    isJump[i] = true;
                }
            }
        }
    }
}
