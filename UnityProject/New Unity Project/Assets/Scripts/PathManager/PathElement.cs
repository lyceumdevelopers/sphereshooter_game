﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.PathManager
{
    public class PathElement
    {
        public const int AdjacentPathElementsCount = 8;

        public Vector3 GroundedPoint = Vector3.zero;
        public PathElement[] AdjacentPathElements = new PathElement[AdjacentPathElementsCount];
        public bool[] IsAdjacentPathElementsOnJumpDistance = new bool[AdjacentPathElementsCount];

        public PathElement(Vector3 groundedPoint)
        {
            GroundedPoint = groundedPoint;
        }

        public PathElement(Vector3 groundedPoint, PathElement[] adjacentPathElements)
        {
            if (groundedPoint == null)
                throw new ArgumentNullException("GroundedPoint is null.");
            if (adjacentPathElements == null)
                throw new ArgumentNullException("AdjacentPathElements is null.");
            if (adjacentPathElements.Length != AdjacentPathElementsCount)
                throw new ArgumentException("AdjacentPathElements is invalid.");

            GroundedPoint = groundedPoint;
            AdjacentPathElements = adjacentPathElements;
            IsAdjacentPathElementsOnJumpDistance = Enumerable.Repeat(false, AdjacentPathElementsCount).ToArray();
        }

        public PathElement(Vector3 groundedPoint, PathElement[] adjacentPathElements, bool[] isAdjacentPathElementsOnJumpDistance)
        {
            if (groundedPoint == null)
                throw new ArgumentNullException("GroundedPoint is null.");
            if (adjacentPathElements == null)
                throw new ArgumentNullException("AdjacentPathElements is null.");
            if (adjacentPathElements.Length != AdjacentPathElementsCount)
                throw new ArgumentException("AdjacentPathElements is invalid.");
            if (isAdjacentPathElementsOnJumpDistance == null)
                throw new ArgumentNullException("IsAdjacentPathElementsOnJumpDistance is null.");
            if (isAdjacentPathElementsOnJumpDistance.Length != AdjacentPathElementsCount)
                throw new ArgumentException("IsAdjacentPathElementsOnJumpDistance is invalid.");

            GroundedPoint = groundedPoint;
            AdjacentPathElements = adjacentPathElements;
            IsAdjacentPathElementsOnJumpDistance = isAdjacentPathElementsOnJumpDistance;
        }

        public PathElement(Vector3 groundedPoint, Vector3[] adjacentPathPoints, bool[] isAdjacentPathElementsOnJumpDistance)
        {
            if (groundedPoint == null)
                throw new ArgumentNullException("GroundedPoint is null.");
            if (adjacentPathPoints == null)
                throw new ArgumentNullException("AdjacentPathPoints is null.");
            if (adjacentPathPoints.Length != AdjacentPathElementsCount)
                throw new ArgumentException("AdjacentPathPoints is invalid.");
            if (isAdjacentPathElementsOnJumpDistance == null)
                throw new ArgumentNullException("IsAdjacentPathElementsOnJumpDistance is null.");
            if (isAdjacentPathElementsOnJumpDistance.Length != AdjacentPathElementsCount)
                throw new ArgumentException("IsAdjacentPathElementsOnJumpDistance is invalid.");

            GroundedPoint = groundedPoint;
            for (int i = 0; i < AdjacentPathElementsCount; i++)
            {
                if (groundedPoint != null)
                {
                    PathElement pathElement = new PathElement(groundedPoint);
                    AdjacentPathElements[i] = pathElement;
                }
            }
            IsAdjacentPathElementsOnJumpDistance = isAdjacentPathElementsOnJumpDistance;
        }
    }
}
