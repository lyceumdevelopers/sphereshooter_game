﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ObjectsDestructible : NetworkBehaviour {
	public enum WallType { Wall, WallWithGlass, WallCorner}
	public WallType _WallType;
	[SyncVar]
	public bool _isActive = false;
	[SyncVar]
	public float _Damage,
				 _Radius;
	[SyncVar]
	public Vector3 _BangPosition;
	GameObject newWall;
	// Use this for initialization
	void Start () {
	
	}
	[Command(channel = 0)]
	void CmdSpawn(){
		switch (_WallType) {
		case WallType.Wall:
			newWall = (GameObject)Instantiate (Resources.Load ("CosmicMap_Wall(Fragmented)"), this.gameObject.transform.position, this.gameObject.transform.rotation);
			break;
		case WallType.WallCorner:
			newWall = (GameObject) Instantiate (Resources.Load ("CosmicMap_WallCorner(Fragmented)"), this.gameObject.transform.position, this.gameObject.transform.rotation);
			break;
		case WallType.WallWithGlass:
			newWall = (GameObject)Instantiate (Resources.Load ("CosmicMap_WallWithGlass(Fragmented)"), this.gameObject.transform.position, this.gameObject.transform.rotation);
			newWall.transform.Rotate (90, 0, 0);
			break;
		default:
			break;
		}
		NetworkServer.Spawn (newWall);
		FragmentsPhysic[] scripts = newWall.GetComponentsInChildren<FragmentsPhysic> ();
		foreach (FragmentsPhysic script in scripts) {
			script._Radius = _Radius;
			script._Damage = _Damage;
			script._BangPosition = _BangPosition;
		}
		NetworkServer.Destroy (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		if (_isActive && isServer) {
			CmdSpawn ();
		}
	}
}