using System;
using UnityEngine;
using UnityEngine.Networking;
namespace UnityStandardAssets.Vehicles.Ball
{
    public class Ball : NetworkBehaviour
    {
        public float max_MovePower = 40;
        public float min_MovePower = 20;
		public float m_MovePower = 20; // The force added to the ball to move it.
		public bool m_UseTorque = true; // Whether or not to use torque to move the ball.
		public float m_MaxAngularVelocity = 25; // The maximum velocity the ball can rotate at.
		public float m_JumpPower = 2; // The force added to the ball when it jumps.
		public float counter = 0;
        public GameObject _Downside;
        public GameObject _Head;
        private float k_GroundRayLength = 1f; // The length of the ray to check if the ball is grounded.
        private Rigidbody m_Rigidbody;

        private void Start()
        {
            k_GroundRayLength = transform.lossyScale.y;
            m_Rigidbody = _Downside.GetComponent<Rigidbody>();
            // Set the maximum angular velocity.
            m_Rigidbody.maxAngularVelocity = m_MaxAngularVelocity;
            m_MovePower = min_MovePower;
        }

        public void Move(Vector3 moveDirection, bool jump)
        {
			if (counter <= 0) {
				counter = 0;
				m_MovePower = min_MovePower;
			}
			if (counter > 0) {
				m_MovePower = max_MovePower;
				counter -= Time.deltaTime;
			}
			// If using torque to rotate the ball...
			if (m_UseTorque) {
				// ... add torque around the axis defined by the move direction.
				m_Rigidbody.AddTorque (new Vector3 (moveDirection.z, 0, -moveDirection.x) * m_MovePower);
			} else {
				// Otherwise add force in the move direction.
				m_Rigidbody.AddForce (moveDirection * m_MovePower);
			}

			// If on the ground and jump is pressed...
            if (jump)
			if (Physics.Raycast (_Downside.transform.position, -Vector3.up, _Downside.transform.lossyScale.z)) {
					// ... add force in upwards.
					m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, 0, m_Rigidbody.velocity.z);
					m_Rigidbody.AddForce (Vector3.up * m_JumpPower, ForceMode.Impulse);
			} 
        }
    }
}
