using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;
namespace UnityStandardAssets.Vehicles.Ball
{
    public class BallUserControl : NetworkBehaviour
    {
        public float _HeadHeight = 3.5F;
        public Transform _Head;
        public Transform _Downside;
        public float _XSpeed = 70,
                _YSpeed = 50,
                _Ymin = -10,
                _Ymax = 60;
        public float _Damping = 3;
        public Vector3 g_v3_AIMoveDirection, g_v3_AILookDirection;
		[SyncVar]
		public bool isControllable = true;

        private Ball ball; // Reference to the ball controller.

        private Vector3 move;
        // the world-relative desired move direction, calculated from the camForward and user input.

        private Vector3 camForward; // The current forward direction of the camera
        private bool p_b_IsJump; // whether the jump button is currently pressed

        private Vector3 last_pos;
        Rigidbody m_Rigidbody;
        private void Awake()
        {
            // Set up the reference.
            ball = GetComponent<Ball>();
            m_Rigidbody = _Downside.GetComponent<Rigidbody>();
            last_pos = gameObject.transform.position;
            p_b_IsAI = gameObject.tag == "AI";
        }

        float _mouseX, _mouseY;
        bool p_b_IsAI;

        private void Update()
        {
			if (this.isLocalPlayer && isControllable && !p_b_IsAI) {
                // Get the axis and jump input.
                float p_f_H, p_f_V;
                p_f_H = CrossPlatformInputManager.GetAxis ("Horizontal");
                p_f_V = CrossPlatformInputManager.GetAxis ("Vertical");
                p_b_IsJump = CrossPlatformInputManager.GetButton ("Jump");

				_mouseX += Input.GetAxis ("Mouse X") * _YSpeed * 0.02F;
				_mouseY -= Input.GetAxis ("Mouse Y") * _XSpeed * 0.02F;
				_mouseY = ClampAngle (_mouseY, _Ymin, _Ymax);

				Quaternion rotation = Quaternion.Lerp (_Head.rotation, Quaternion.Euler (_mouseY, _mouseX, 0), _Damping * Time.deltaTime);
				_Head.rotation = rotation;

				// calculate camera relative direction to move:
				camForward = Vector3.Scale (_Head.forward, new Vector3 (1, 0, 1)).normalized;
				move = (p_f_V * camForward + p_f_H * _Head.right).normalized;

                _Head.rotation = rotation;
                camForward = Vector3.Scale(_Head.forward, new Vector3(1, 0, 1)).normalized;
                move = (p_f_V * camForward + p_f_H * _Head.right).normalized;
            }

            if (isServer && p_b_IsAI && isControllable)
            {
                move = new Vector3(g_v3_AIMoveDirection.x, 0, g_v3_AIMoveDirection.z).normalized;    
                _Head.rotation = Quaternion.LookRotation(g_v3_AILookDirection);
            }
            Vector3 headPosition = _Downside.transform.position;
            headPosition.y += _HeadHeight;
            _Head.position = headPosition;
        }

        private void FixedUpdate()
        {
            // Call the Move function of the ball controller
            if (isLocalPlayer || isServer && p_b_IsAI)
            {
                ball.Move(move, p_b_IsJump);
                p_b_IsJump = false;

                if (Physics.Raycast(_Head.transform.position, Vector3.up, _Head.transform.lossyScale.z))
                {
                    if (m_Rigidbody.velocity.y > 0)
                        m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, 0, m_Rigidbody.velocity.z);
                }
            }
        }

        private float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360)
                angle += 360;
            if (angle > 360)
                angle -= 360;
            return Mathf.Clamp(angle, min, max);
        }
    }
}
