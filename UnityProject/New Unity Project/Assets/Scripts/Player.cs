﻿using UnityEngine;
using System.Collections;
using System;

public class Player : MonoBehaviour {
    public Guid g_g_Guid = Guid.Empty;
    public Guid g_g_KillerGuid = Guid.Empty;
    public int Kills = 0;
    public int Deaths = 0;
    public string Name = "";
    GameStatus p_scr_GameStatus;
    // Use this for initialization
    void Start () {
        p_scr_GameStatus = GameObject.Find("Container").GetComponent<GameStatus>();
        Name = gameObject.name;
    }

    public void Dead()
    {
        p_scr_GameStatus.Death(g_g_Guid, g_g_KillerGuid);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
