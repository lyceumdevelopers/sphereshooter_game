﻿using UnityEngine;
using System.Collections;

public class FragmentsPhysic : MonoBehaviour {
	bool setted = false;
	public float _Damage,
				 _Radius;
	public Vector3 _BangPosition;
	Rigidbody _rigidbody;
	// Use this for initialization
	void Start () {
	
	}
	// Update is called once per frame
	void Update () {
		if (!setted) {
			setted = true;
			_rigidbody = gameObject.AddComponent<Rigidbody> ();
			_rigidbody.AddExplosionForce (_Damage, _BangPosition, _Radius);
		}
		if (gameObject.transform.position.y <= -10) {
			Destroy (gameObject);
		}
	}
}
