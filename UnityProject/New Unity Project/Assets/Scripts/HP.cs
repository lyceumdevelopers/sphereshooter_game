﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using UnityStandardAssets.ImageEffects;

public class HP : NetworkBehaviour {
    public float _MaxHP = 100;
	public int _HP = 100;
	public int _MaxAMMO = 100;
    public int _AMMO = 100;
	public int _EXP = 0;
    public GameObject _FlareParticle;
    public GameObject _SmokeParticle;
	public Transform _Downside;
	public Texture lifeBoxTexture, lifeStatusTexture, ammoBoxTexture, ammoStatusTexture;
	public UnitType unitType;
	public enum UnitType { Rocketeer, Demoman, Sniper, Gunner};
	[SyncVar]
	public bool p_b_IsDead = false;
    public Guid p_g_PlayerGuid = Guid.Empty;
    bool p_b_IsAI;
    float p_f_UntouchableTime = 5,
          p_f_StartTime = 0;
    int p_i_LastHP = 0,
        p_i_LastAmmo = 0;
    BloomOptimized p_scr_Bloom;
    Player p_scr_Player;
    // Use this for initialization
    void Start () {
        p_scr_Player = gameObject.GetComponent<Player>();

		float p_f_Pixel = Screen.height / 30;
		p_r_LifeBoxRect = new Rect (p_f_Pixel, Screen.height - p_f_Pixel * 3, p_f_Pixel * 10, p_f_Pixel);
        p_r_AmmoBoxRect = new Rect(p_f_Pixel, Screen.height - p_f_Pixel * 4.5F, p_f_Pixel * 10, p_f_Pixel);
        p_r_LifeStatusRect = p_r_LifeBoxRect;
        p_r_AmmoStatusRect = p_r_AmmoBoxRect;

		p_r_LabelBox = new Rect ((Screen.width / 2) - p_f_Pixel * 15, (Screen.height / 2) - p_f_Pixel * 5, p_f_Pixel * 30, p_f_Pixel * 10);

		p_s_Style = new GUIStyle ();        
        p_s_Style.fontStyle = FontStyle.Bold;
		p_s_Style.fontSize = Convert.ToInt32(p_f_Pixel * 5);

        p_b_IsAI = gameObject.tag == "AI";
        if (isServer && p_b_IsAI && p_scr_Player.g_g_Guid == Guid.Empty)
        {
            p_scr_Player.g_g_Guid = Guid.NewGuid();
        }

        p_f_StartTime = Time.time;
        p_i_LastHP = _HP;
        p_i_LastAmmo = _AMMO;
        p_scr_Bloom = gameObject.GetComponentInChildren<BloomOptimized>();
    }
	
	// Update is called once per frame
	void Update () {
		if (isLocalPlayer || (p_b_IsAI && isServer)) {
            if (_HP < p_i_LastHP || p_i_LastAmmo > _AMMO)
            {
                p_i_LastHP = _HP;
                p_i_LastAmmo = _AMMO;
                p_scr_Bloom.intensity = 30;
            }
            p_scr_Bloom.intensity = Mathf.Lerp(0.75F, p_scr_Bloom.intensity, Time.deltaTime * 25);

            if (p_b_IsDead) {
				deadTime += Time.deltaTime;
            }

            if (_AMMO < 0)
            {
                _AMMO = 0;
            }
            if (_HP < 0)
            {
                _HP = 0;
            }

			p_r_LifeStatusRect.width = Mathf.Lerp (p_r_LifeStatusRect.width, (p_r_LifeBoxRect.width * _HP) / _MaxHP, Time.deltaTime * 5);
            p_r_AmmoStatusRect.width = Mathf.Lerp(p_r_AmmoStatusRect.width, (p_r_AmmoBoxRect.width * _AMMO) / _MaxAMMO, Time.deltaTime * 5);

            if (p_b_IsDead && deadTime > 2) {
				CmdRespawn ();
			}
        }
		if (isServer) {
            if (_Downside.transform.position.y < -30)
                CmdDamage(1000, p_scr_Player.g_g_KillerGuid);
		}
    }

	Rect p_r_LifeBoxRect, p_r_LifeStatusRect, p_r_LabelBox, p_r_AmmoBoxRect, p_r_AmmoStatusRect;
	GUIStyle p_s_Style;

	float deadTime;

	void OnGUI() {
		if (isLocalPlayer) 
		{
			if (p_b_IsDead) {
                GUI.Label (p_r_LabelBox, "Вы мертвы: " + (int)(deadTime + 1), p_s_Style);
			} else {
                GUI.DrawTexture(p_r_LifeBoxRect, lifeBoxTexture);
                GUI.DrawTexture(p_r_LifeStatusRect, lifeStatusTexture);
                GUI.DrawTexture (p_r_AmmoBoxRect, ammoBoxTexture);
				GUI.DrawTexture (p_r_AmmoStatusRect, ammoStatusTexture);
                GUI.contentColor = Color.black;
                GUI.Label(p_r_LifeBoxRect, "HP:" + Convert.ToString(_HP) + "/" + Convert.ToString(_MaxHP));
                GUI.Label(p_r_AmmoBoxRect, "ARMOR:" + Convert.ToString(_AMMO) + "/" + Convert.ToString(_MaxAMMO));
            }
		}
	}

	[Command(channel = 0)]
	void CmdRespawn()
	{
        if (!p_b_IsAI)
        {
            GameObject obj = (GameObject)Instantiate(Resources.Load(@"Players\Camera"));
            obj.GetComponent<Player>().g_g_Guid = gameObject.GetComponent<Player>().g_g_Guid;
            NetworkServer.Spawn(obj);
            NetworkServer.ReplacePlayerForConnection(connectionToClient, obj, playerControllerId);
        }
        else
        {
            GameObject[] p_go_StartPoses = GameObject.FindGameObjectsWithTag("SpawnPos");
            GameObject p_go_Obj = null;
            System.Random p_r_Rand = new System.Random();
            int p_i_RandIndex = p_r_Rand.Next(1, 5);
            if (p_i_RandIndex == 1)
                p_go_Obj = (GameObject)Instantiate(Resources.Load(@"Players\AIRocketeer"));
            if (p_i_RandIndex == 2)
                p_go_Obj = (GameObject)Instantiate(Resources.Load(@"Players\AISniper"));
            if (p_i_RandIndex == 3)
                p_go_Obj = (GameObject)Instantiate(Resources.Load(@"Players\AIDemoman"));
            if (p_i_RandIndex == 4)
                p_go_Obj = (GameObject)Instantiate(Resources.Load(@"Players\AIGunner"));
            p_go_Obj.transform.position = p_go_StartPoses[p_r_Rand.Next(p_go_StartPoses.GetLength(0) - 1)].transform.position;
            p_go_Obj.GetComponent<Player>().g_g_Guid = p_scr_Player.g_g_Guid;
            NetworkServer.Spawn(p_go_Obj);
        }

        NetworkServer.Destroy(gameObject);
    }

	[Command(channel = 0)]
	public void CmdDamage(float val, Guid i_g_Killer)
	{
        if (Time.time - p_f_StartTime > p_f_UntouchableTime)
            if (!p_b_IsDead)
            {            
                if (_AMMO <= 0)
                {
                    _AMMO = 0;
                    _HP -= (int)val;
                }
                else
                {
                    _AMMO -= (int)val;
                }

                if (_HP < 40)
                {
                    if (_FlareParticle != null)
                        _FlareParticle.SetActive(true);
                    if (_HP <= 0)
                    {
                        _HP = 0;
                        if (_SmokeParticle != null)
                            _SmokeParticle.SetActive(true);
                        Death();
                    }
                }

                p_scr_Player.g_g_KillerGuid = i_g_Killer;
            }
	}

    [Command(channel = 0)]
    public void CmdMakeUncontrollable()
    {
        MakeUncontrollable();
        RpcMakeUncontrollable();
    }

    [ClientRpc(channel = 0)]
    void RpcMakeUncontrollable()
    {
        MakeUncontrollable();
    }

    void MakeUncontrollable()
    {
        gameObject.GetComponent<WeaponController>().g_b_IsControllable = false;
    }

    void Death()
    {	
		p_b_IsDead = true;
		gameObject.GetComponent<UnityStandardAssets.Vehicles.Ball.BallUserControl> ().isControllable = false;
		GameObject bang = Instantiate(Resources.Load("Bang")) as GameObject;
		bang.transform.position = _Downside.transform.position;
        p_scr_Player.Dead();
        CmdMakeUncontrollable();
        NetworkServer.Spawn (bang);
    }
}
