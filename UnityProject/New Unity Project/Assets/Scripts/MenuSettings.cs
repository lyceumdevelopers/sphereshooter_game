﻿using UnityEngine;
using System.Collections;

public class MenuSettings : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Cursor.visible = false;
    }
    bool pressed = true;
	// Update is called once per frame
	void Update () {
	    if (Input.GetAxis("Cancel") > 0)
        {
            pressed = true;
        }
        if (Input.GetAxis("Cancel") == 0)
        {
            if (pressed)
            {
                Cursor.visible = !Cursor.visible;
            }
            pressed = false;
        }
    }
}
