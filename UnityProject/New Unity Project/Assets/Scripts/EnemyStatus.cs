﻿using UnityEngine;
using System.Collections;

public class EnemyStatus : MonoBehaviour {
    public bool IsInvisible = false;

    bool IsSniper = false;
    SniperWeaponController p_scr_SniperWeaponController;
    Light light;
	// Use this for initialization
	void Start () {
        p_scr_SniperWeaponController = gameObject.GetComponent<SniperWeaponController>();
        if (p_scr_SniperWeaponController != null)
        {
            IsSniper = true;
        }
        light = gameObject.GetComponentInChildren<Light>();
    }

    // Update is called once per frame
    void Update() {
        if (IsSniper)
        {
            IsInvisible = p_scr_SniperWeaponController.g_b_IsInvisible;
        }

        light.enabled = !IsInvisible;
	}
}
