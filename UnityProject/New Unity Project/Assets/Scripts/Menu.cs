﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.IO;
using UnityEngine.UI;

public class Menu : NetworkBehaviour {
    public GUIStyle _ButtonsStyle;
    const float _WIDTH = 250, _HEIGHT = 50;
    const int NETWORK_PORT = 4585;
    const int MAX_CONNECTIONS = 20;
    const bool USE_NAT = false;
	NetworkManager manager;
    int mapIndex = 0;
    string[] mapNames = new string[2];
                                // Use this for initialization
    void Start () {
		manager = GameObject.Find ("NetworkManager").GetComponent<NetworkManager> ();
        for (int i = 0; i < _Button.GetLength(0); i++)
        {
			_Button[i] = new Rect(Screen.width / 2 - _WIDTH / 2, 25 + (i * (_HEIGHT + 10)), _WIDTH, _HEIGHT);
        }
		_MenyStatus = MenuStatus.Mine;
        mapNames[0] = "CosmicMap";
        mapNames[1] = "SciFi level";
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    enum MenuStatus { Mine, Singleplayer, Client, ClientConnection, Server, Multiplayer, Options, UnitSelection, Network, Tutorial, None}
    MenuStatus _MenyStatus = MenuStatus.Mine;

    public enum Language { Russian, English }
    public Language _Language = Language.Russian;

    Rect[] _Button = new Rect[5];
	string tutorial_str;
    string host = "localhost", name = "Player";
	int unit = 0;

    void OnGUI()
    {
        switch (_MenyStatus)
        {
            case MenuStatus.Mine:
                    switch (_Language)
                    {
                        case Language.English:

                        if (GUI.Button(_Button[0], "Singleplayer", _ButtonsStyle))
                        {
                        _MenyStatus = MenuStatus.Singleplayer;
                        }
                        if (GUI.Button(_Button[1], "Multiplayer", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Multiplayer;
                        }
                        if (GUI.Button(_Button[2], "Options", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Options;
                        }
						if (GUI.Button(_Button[3], "Tutorial", _ButtonsStyle))
						{
							_MenyStatus = MenuStatus.Tutorial;
						}
                        if (GUI.Button(_Button[4], "Exit", _ButtonsStyle))
                        {
                            Application.Quit();
                        }
                        break;
                        case Language.Russian:

                        if (GUI.Button(_Button[0], "Одиночная игра", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Singleplayer;
                        }
                        if (GUI.Button(_Button[1], "Сетевая игра", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Multiplayer;
                        }
                        if (GUI.Button(_Button[2], "Настройки", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Options;
                        }
						if (GUI.Button(_Button[3], "Обучение", _ButtonsStyle))
						{
							_MenyStatus = MenuStatus.Tutorial;
						}
                        if (GUI.Button(_Button[4], "Выйти", _ButtonsStyle))
                        {
                            Application.Quit();
                        }
                        break;
                    }
                break;
            case MenuStatus.Singleplayer:
                switch (_Language)
                {
                    case Language.English:
						if (GUI.Button(_Button[0], "Начать", _ButtonsStyle))
						{
							Create ();
						}
                        if (GUI.Button(_Button[1], "Back", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Mine;
                        }
                        break;
                    case Language.Russian:
						if (GUI.Button(_Button[0], "Начать", _ButtonsStyle))
						{
							Create ();
						}
                        if (GUI.Button(_Button[1], "Назад", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Mine;
                        }
                        break;
                }
                break;
            case MenuStatus.Client:
			host = GUI.TextField(_Button[0], host);
                switch (_Language)
                {
                    case Language.English:
                        if (GUI.Button(_Button[1], "Connect", _ButtonsStyle))
                        {
                            Connect();
                        }
                        if (GUI.Button(_Button[2], "Back", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Multiplayer;
                        }
                        break;
                    case Language.Russian:
                        if (GUI.Button(_Button[1], "Подключиться", _ButtonsStyle))
                        {
                            Connect();
                        }
                        if (GUI.Button(_Button[2], "Назад", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Multiplayer;
                        }
                        break;
                }
                break;
            case MenuStatus.Server:
                switch (_Language)
                {
                    case Language.English:
                        mapIndex = GUI.SelectionGrid(_Button[0], mapIndex, mapNames, 1);
                        if (GUI.Button(_Button[1], "Create", _ButtonsStyle))
                        {
							Create();
                        }
                        if (GUI.Button(_Button[2], "Back", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Multiplayer;
                        }
                        break;
                    case Language.Russian:
                        mapIndex = GUI.SelectionGrid(_Button[0], mapIndex, mapNames, 1);
                        if (GUI.Button(_Button[1], "Создать", _ButtonsStyle))
                        {
							Create();
                        }
                        if (GUI.Button(_Button[2], "Назад", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Multiplayer;
                        }
                        break;
                }
                break;
            case MenuStatus.Multiplayer:
                switch (_Language)
                {
                    case Language.English:
                        if (GUI.Button(_Button[0], "Server", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Server;
                        }
                        if (GUI.Button(_Button[1], "Client", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Client;
                        }
                        if (GUI.Button(_Button[2], "Back", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Mine;
                        }
                        break;
                    case Language.Russian:
                        if (GUI.Button(_Button[0], "Сервер", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Server;
                        }
                        if (GUI.Button(_Button[1], "Клиент", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Client;
                        }
                        if (GUI.Button(_Button[2], "Назад", _ButtonsStyle))
                        {
                            _MenyStatus = MenuStatus.Mine;
                        }
                        break;
                }
                break;
		case MenuStatus.Options:
			switch (_Language) {
			case Language.English:
				if (GUI.Button (_Button [0], "Back", _ButtonsStyle)) {
					_MenyStatus = MenuStatus.Mine;
				}
				break;
			case Language.Russian:
				if (GUI.Button (_Button [0], "Назад", _ButtonsStyle)) {
					_MenyStatus = MenuStatus.Mine;
				}
				break;
			}
			break;
		case MenuStatus.ClientConnection:
			switch (_Language) {
			case Language.English:
				if (GUI.Button (_Button [0], "Cancel", _ButtonsStyle)) {
					_MenyStatus = MenuStatus.Client;
					manager.StopClient ();
				}
				break;
			case Language.Russian:
				if (GUI.Button (_Button [0], "Отмена", _ButtonsStyle)) {
					_MenyStatus = MenuStatus.Client;
					manager.StopClient ();
				}
				break;
			}
			break;
		default:
			break;
		case MenuStatus.Network:
			switch (_Language) {
			case Language.English:
				GUI.TextField (_Button [0], name);
				if (GUI.Button (_Button [1], "OK", _ButtonsStyle)) {
					ChangeName ();
					_MenyStatus = MenuStatus.Options;
				}
				if (GUI.Button (_Button [2], "Back", _ButtonsStyle)) {
					_MenyStatus = MenuStatus.Options;
				}
				break;
			case Language.Russian:
				GUI.TextField (_Button [0], name);
				if (GUI.Button (_Button [1], "Принять", _ButtonsStyle)) {
					ChangeName ();
					_MenyStatus = MenuStatus.Options;
				}
				if (GUI.Button (_Button [2], "Назад", _ButtonsStyle)) {
					_MenyStatus = MenuStatus.Options;
				}
				break;
			}
			break;
		case MenuStatus.Tutorial:
			Application.OpenURL (Path.Combine (Application.dataPath, "tutorial.txt"));
			_MenyStatus = MenuStatus.Mine;
			break;
        }
    }

	void ChangeName() 
	{
		using (StreamWriter writer = new StreamWriter(Path.Combine(Application.dataPath, "name.txt")))
			{
				writer.WriteLine (name);
			}
	}

    void Create()
    {
        manager.onlineScene = mapNames[mapIndex];
		manager.StartHost ();
    }

	void OnServerError()
	{
		_MenyStatus = MenuStatus.Server;
	}

	void OnClientError()
	{
		_MenyStatus = MenuStatus.Client;
	}

    void Connect()
    {
		manager.networkAddress = host;
		manager.StartClient ();
    }

    void OnFailedToConnect(NetworkConnectionError error)
    {
        Debug.Log("Failed to connect: " + error.ToString());
		_MenyStatus = MenuStatus.Mine;
    }

    void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        if (Network.isClient)
        {
            Debug.Log("Disconnected from server: " + info.ToString());
        }
        else {
            Debug.Log("Connections closed");
        }
		_MenyStatus = MenuStatus.Mine;
    }

    void OnConnectedToServer()
    {
        Debug.Log("Connected to server");
    }
}
