﻿using UnityEngine;
using System.Collections;

public class CameraRandomMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
        _rigidbody = gameObject.GetComponent<Rigidbody>();
	}

    Rigidbody _rigidbody;
	
	// Update is called once per frame
	void FixedUpdate () {
        if (_rigidbody.velocity.magnitude < 0.1)
        {
            _rigidbody.AddForce(new Vector3(Random.Range(-0.1F, 0.1F), Random.Range(-0.1F, 0.1F), Random.Range(-0.1F, 0.1F)));
        }
        else
        {
            _rigidbody.AddForce(-_rigidbody.velocity);
        }
        if (_rigidbody.angularVelocity.magnitude < 0.1)
        {
            _rigidbody.AddTorque(new Vector3(Random.Range(-0.1F, 0.1F), Random.Range(-0.1F, 0.1F), Random.Range(-0.1F, 0.1F)));
        }
        else
        {
            _rigidbody.AddTorque(-_rigidbody.angularVelocity);
        }
    }
}
