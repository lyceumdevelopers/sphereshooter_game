﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

public class RocketFly : NetworkBehaviour {
	[SyncVar]
    public Vector3 g_v3_Direction;
    public float _FlySpeed = 10, _MaxDistance = 200, _Radius = 15, _Damage = 100;
	[SyncVar]
    public string g_s_ID;
    public Guid g_g_KillerGuid;
    Rigidbody _rigidbody;
    Vector3 _startPos;
	// Use this for initialization
	void Start () {
        _rigidbody = gameObject.GetComponent<Rigidbody>();
        _startPos = gameObject.transform.position;
        transform.rotation = Quaternion.LookRotation(g_v3_Direction);
    }
	
    void FixedUpdate()
    {
        if (Vector3.Distance(_startPos, transform.position) > _MaxDistance)
        {
            Destroy(gameObject);
        }
        _rigidbody.AddForce(g_v3_Direction * _FlySpeed, ForceMode.VelocityChange);
    }

	// Update is called once per frame
	void Update () {
        
	}

    void OnCollisionEnter(Collision col)
    {
		if (isServer) 
		{
			if (col.gameObject != null)
			if (col.gameObject.tag == "Player")
			if (col.gameObject.GetComponentInParent<NetworkIdentity> ().netId.ToString () == g_s_ID) 
			{
				return;
			}

			Collider[] targets = Physics.OverlapSphere (transform.position, _Radius);
			foreach (Collider current in targets) {
				try {
					if (current.gameObject != null) {
						if (current.gameObject.tag == "Player") {
                            current.gameObject.GetComponentInParent<HP>().CmdDamage(_Damage / Vector3.Distance(transform.position, current.transform.position), g_g_KillerGuid);
                        }
						if (current.gameObject.tag == "Destroyable") {
							ObjectsDestructible script = current.gameObject.GetComponent<ObjectsDestructible> ();
							script._Damage = _Damage;
							script._Radius = _Radius;
							script._isActive = true;
							script._BangPosition = transform.position;
						}
                        if (current.gameObject.tag == "Mine")
                        {
                            current.gameObject.GetComponent<Mine>()._IsPressed = true;
                        }
                    }
				} catch {
				}
			}

			GameObject bang = Instantiate (Resources.Load ("Bang")) as GameObject;
			bang.transform.position = gameObject.transform.position;
			NetworkServer.Spawn (bang);
			NetworkServer.Destroy (gameObject);
		}
    }
}
