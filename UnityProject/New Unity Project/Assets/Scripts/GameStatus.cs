﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;
using UnityStandardAssets.CrossPlatformInput;

public class GameStatus : NetworkBehaviour {

	Rect[] _NameTables = new Rect[8];
    Rect[] _KillsTables = new Rect[8];
    Rect[] _DeathsTables = new Rect[8];
    Player[] p_a_Players;
    bool p_b_IsShow = false;
	// Use this for initialization
	void Start () {
		float pixel = Screen.width / 30;
		for (int i = 0; i < _NameTables.GetLength (0); i++) {
			_NameTables [i] = new Rect (pixel / 4, pixel * i, pixel * 2.5F, pixel / 2);
            _KillsTables[i] = new Rect((pixel / 4) + pixel * 2.5F, pixel * i, pixel, pixel / 2);
            _DeathsTables[i] = new Rect((pixel / 4) + pixel * 3.5F, pixel * i, pixel, pixel / 2);
        }

        if (isServer)
            p_a_Players = GameObject.FindObjectsOfType<Player>();
    }

	public void Death(Guid i_g_DeadGuid, Guid i_g_KillerGuid)
	{
		foreach (Player player in p_a_Players) {
			if (player.g_g_Guid == i_g_DeadGuid) {
				player.Deaths++;
            }
			if (player.g_g_Guid == i_g_KillerGuid && i_g_DeadGuid != i_g_KillerGuid) {
				player.Kills++;
            }
		}
        QuickSort(p_a_Players, 0, p_a_Players.GetLength(0) - 1);
    }


    void OnGUI() {
        if (p_b_IsShow)
        {
            int p_i_Max = 0;
            if (p_a_Players.GetLength(0) < _NameTables.GetLength(0))
            {
                p_i_Max = p_a_Players.GetLength(0);
            }
            else
            {
                p_i_Max = _NameTables.GetLength(0);
            }
            GUI.Label(_NameTables[0], "Name");
            GUI.Label(_KillsTables[0], "Kills");
            GUI.Label(_DeathsTables[0], "Deaths");
            for (int i = 0; i < p_i_Max - 1; i++)
            {
                GUI.Label(_NameTables[i + 1], p_a_Players[p_a_Players.GetLength(0) - 1 - i].Name);
                GUI.Label(_KillsTables[i + 1], Convert.ToString(p_a_Players[p_a_Players.GetLength(0) - 1 - i].Kills));
                GUI.Label(_DeathsTables[i + 1], Convert.ToString(p_a_Players[p_a_Players.GetLength(0) - 1 - i].Deaths));
            }
        }
	}

    void Update()
    {
        p_b_IsShow = Input.GetKey(KeyCode.Tab);
    }

    void QuickSort(Player[] a, int l, int r)
    {
        Player temp;
        int x = a[l + (r - l) / 2].Kills - a[l + (r - l) / 2].Deaths;

        int i = l;
        int j = r;

        while (i <= j)
        {
            while (a[i].Kills - a[i].Deaths < x) i++;
            while (a[j].Kills - a[j].Deaths > x) j--;
            if (i <= j)
            {
                temp = a[i];
                a[i] = a[j];
                a[j] = temp;
                i++;
                j--;
            }
        }
        if (i < r)
            QuickSort(a, i, r);

        if (l < j)
            QuickSort(a, l, j);
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
        p_a_Players = GameObject.FindObjectsOfType<Player>();
    }

    void OnPlayerConnect(NetworkPlayer player)
    {
        p_a_Players = GameObject.FindObjectsOfType<Player>();
    }
}
