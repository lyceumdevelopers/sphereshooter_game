﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {

    public Transform _Head;
    public Transform _Downside;
    public float _HeadHeight,
                _HorizontalSpeed = 120,
                _VerticalSpeed = 250,
                _WalkSpeed,
                _Damping = 6,
                _YMinLimit = 40,
                _YMaxLimit = 80,
                _JumpHeight = 30,
                _Gravitation = 25;

    Rigidbody _rigidbody;
    float _xInput, _yInput, _mouseX, _mouseY, _jumpInput;
    bool _isJump = false;
	// Use this for initialization
	void Start ()
    {
        _rigidbody = _Downside.GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 headPosition = _Downside.position;
        headPosition.y += _HeadHeight;
        _Head.position = headPosition;

        _mouseX += Input.GetAxis("Mouse X") * _VerticalSpeed * 0.02F;
        _mouseY -= Input.GetAxis("Mouse Y") * _HorizontalSpeed * 0.02F;
        _mouseY = ClampAngle(_mouseY, _YMinLimit, _YMaxLimit);

        Quaternion rotation = Quaternion.Lerp(_Head.rotation, Quaternion.Euler(_mouseY, _mouseX, 0), _Damping * Time.deltaTime);
        _Head.rotation = rotation;

        _xInput = Input.GetAxis("Horizontal");
        _yInput = Input.GetAxis("Vertical");
        _jumpInput = Input.GetAxis("Jump");

        Vector3 movementDirection = Vector3.down;

        Vector3 forward = _Head.transform.TransformDirection(Vector3.forward);
        forward.y = 0;
        forward = forward.normalized;
        Vector3 right = new Vector3(forward.z, 0, -forward.x);

        if (_xInput!=0||_yInput!=0)
        {
            movementDirection = (float)_xInput * right + (float)_yInput * forward;
        }

        if (_jumpInput > 0)
        {
            if (!_isJump)
            {
                _rigidbody.AddForce(Vector3.up * _JumpHeight * 10, ForceMode.Acceleration);
                _isJump = true;
            }
        }

        movementDirection *= Time.deltaTime * _WalkSpeed;

        _rigidbody.AddForce(movementDirection * 50, ForceMode.Acceleration);
	}

    void OnCollisionEnter()
    {
        _isJump = false;
    }

    void OnCollisionStay()
    {
        _isJump = false;
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);

    }
}
